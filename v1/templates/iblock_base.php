<?="<?php\n"?>
/**
 * Class <?="$class\n"?>
 */

namespace App\Element\Base;
use Start\IblockElement;

abstract class <?=$class?> extends IblockElement {

    protected $iblockCode = '<?=$code?>';

<?foreach ($fields as $val):?>
	public function set<?=\Start\Utils::camelizeText($val)?>($val){
		$this->setField('<?=$val?>', $val);
		return $this;
	}

<?endforeach?>
<?foreach ($props as $code=>$prop):  $code = in_array($code, $fields) ? $code . '_prop' : $code;
    $descr1 = ($prop['WITH_DESCRIPTION']=='Y') ? ', $descr=\'\'' : '';
    $descr2 = $descr1 ? ', $descr' : '';

?>
    public function set<?=\Start\Utils::camelizeText($code)?>($val<?=$descr1?>){
        $this->setProperty('<?=$code?>', $val<?=$descr2?>);
        return $this;
    }
<?if ($prop['PROPERTY_TYPE'] == 'L'):?>
    public function set<?=\Start\Utils::camelizeText($code)?>ByXmlId($xmlId<?=$descr1?>){
        $this->setPropertyByXmlId('<?=$code?>', $xmlId<?=$descr2?>);
        return $this;
    }

    public function set<?=\Start\Utils::camelizeText($code)?>ByValue($value<?=$descr1?>){
        $this->setPropertyByValue('<?=$code?>', $value<?=$descr2?>);
        return $this;
    }
<?endif?>

<?endforeach?>
}