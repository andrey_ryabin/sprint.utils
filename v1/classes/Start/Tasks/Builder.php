<?php

namespace Start\Tasks;
use Start\Task;
use Start\Utils;
use Start\Storages\Iblock as IblockStorage;

class Builder extends Task {

	protected $buildPathElem = '';
	protected $pathTempl = '';

    protected $iblock;


	public function __construct(){
		parent::__construct();

        $this->ibStorage = new IblockStorage();

        if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/local/')){
            $this->buildPathBase = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/App/Element/Base/';
            $this->buildPathElem = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/App/Element/';
        } else {
            $this->buildPathBase = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/classes/App/Element/Base/';
            $this->buildPathElem = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/classes/App/Element/';
        }

        if (is_file($_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_element.php')){
            $this->elementTemplate = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_element.php';
        } else {
            $this->elementTemplate = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/start/templates/iblock_element.php';
        }

        if (is_file($_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_element.php')){
            $this->elementTemplate = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_element.php';
        } else {
            $this->elementTemplate = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/start/templates/iblock_element.php';
        }

        if (is_file($_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_base.php')){
            $this->baseTemplate = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/start/templates/iblock_base.php';
        } else {
            $this->baseTemplate = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/start/templates/iblock_base.php';
        }


        if (!is_dir($this->buildPathElem)){
            mkdir($this->buildPathElem, BX_DIR_PERMISSIONS, true);
        }

        if (!is_dir($this->buildPathBase)){
            mkdir($this->buildPathBase, BX_DIR_PERMISSIONS, true);
        }
	}

    public function executeBuild($iblockCode = ''){
        if (!empty($iblockCode) && $this->ibStorage->getIdByCode($iblockCode)){
            $this->createBuildBase($iblockCode);
            $this->createBuildElement($iblockCode);

        }else {
            $list = $this->ibStorage->getList();
            foreach ($list as $aItem){
                $this->createBuildBase($aItem['CODE']);
                $this->createBuildElement($aItem['CODE']);
            }
        }

    }


    protected function createBuildBase($iblockCode){
		$class = Utils::camelizeText($iblockCode);
        $file = $this->buildPathBase . $class . '.php';

		$content = Utils::renderTemplate($this->baseTemplate, array(
			'class' => $class,
			'code' => $iblockCode,
			'fields' => $this->getFieldCodes(),
			'props' => $this->ibStorage->getProperties($iblockCode, 'CODE')

		));

		file_put_contents($file, $content);
		$this->out('created [cyan]%s.php[/]', $class);
	}

    protected function createBuildElement($iblockCode){
		$class = Utils::camelizeText($iblockCode);

        $file = $this->buildPathElem . $class .'.php';

		if (file_exists($file)){
			$this->out('already exists [green]%s.php[/]', $class);

		} else {
			$content = Utils::renderTemplate($this->elementTemplate, array(
				'class' => $class,
            ));

			file_put_contents($file, $content);
			$this->out('created [purple]%s.php[/]', $class);

		}

	}

    protected function getFieldCodes(){
        return array(
            'CODE',
            'NAME',
            'IBLOCK_SECTION_ID',
            'ACTIVE',
            'DATE_ACTIVE_FROM',
            'DATE_ACTIVE_TO',
            'SORT',
            'PREVIEW_TEXT',
            'DETAIL_TEXT',
            'TAGS',
            'PREVIEW_PICTURE',
            'DETAIL_PICTURE'
        );
    }
}