<?php

namespace Start\Tasks;
use Start\Task;


class Component extends Task {


	public function executeCacheInfo(){
		$size = 0;

		$out = \Start\Component::getCacheInfo();
		foreach ($out  as $v){
			$this->out('%s %s', date('d.m.Y H:i:s', $v['atime']), $v['file']);
			$size += $v['size'];
		}


		$index = 0;
		$iec = array("B", "Kb", "Mb", "Gb", "Tb");

		while (($size/1024)>1) {
			$size=$size/1024;
			$index++;
		}

		$this->out(round($size,1)." ".$iec[$index]);
	}

}