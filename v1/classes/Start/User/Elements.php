<?php

namespace Start\User;

class Elements {

	protected $aOrder = array(
        'timestamp_x' => 'desc',
    );

    protected $aFilter = array();
    protected $aSelect = array();
	protected $aNavParams = false;

    private $dbResult = null;


    /* @return Elements */
	public static function create(){
        $class = get_called_class();
        return new $class;
	}

    /* @return ElementsDbResult */
    public function execute(){
        if (empty($this->dbResult)){
            $this->dbResult = $this->createDbResult();
        }
        return $this->dbResult;
    }


    /* @return \CDBResult */
    protected function createDbResult(){
        $by = current(array_keys($this->aOrder));
        $order = $this->aOrder[$by];

        $aParams = array();

        if (!empty($this->aSelect)){

            $aFields = $aSelect = array();
            foreach ($this->aSelect as $val){
                if (0 === strpos($val, 'UF')){
                    $aSelect[] = $val;
                } else {
                    $aFields[] = $val;
                }
            }

            if (!empty($aSelect)){
                $aParams['SELECT'] = $aSelect;
            }

            if (!empty($aFields)){
                $aParams['FIELDS'] = $aFields;
            }
        }

        if (!empty($this->aNavParams)){
            $aParams['NAV_PARAMS'] = $this->aNavParams;
        }


        $class = get_class($this) . 'DbResult';
        $class = class_exists($class, true) ? $class : 'Start\\User\\ElementsDbResult';

        $dbResult = \CUser::GetList($by,$order, $this->aFilter, $aParams);
        return new $class($dbResult);
    }

    public function mergeFilter($aFilter){
        $this->aFilter = array_merge($this->aFilter, $aFilter);
		return $this;
    }

    public function mergeSelect($aSelect){
        $this->aSelect = array_merge($this->aSelect, $aSelect);
		return $this;
    }

    public function setOrder($aOrder){
        $this->aOrder = $aOrder;
		return $this;
    }

	public function setGroupIds($ids){
		$this->aFilter['GROUPS_ID'] = $this->prepareIds($ids);
		return $this;
	}

    public function setIds($ids){
        $this->aFilter['ID'] = $this->prepareUserIds($ids);
		return $this;
    }

    public function setActive(){
        $this->aFilter['ACTIVE'] = 'Y';
        return $this;
    }

    public function setNavTopCount($topLimit = false){
        if ($topLimit){
            $this->aNavParams = array('nTopCount' => $topLimit);
        } else {
            $this->aNavParams = false;
        }
		return $this;
    }

    public function setNavPageSize($pageLimit, $pageStart = false){
        if ($pageStart){
            $this->aNavParams = array('iNumPage' => $pageStart, "nPageSize" => $pageLimit,"bShowAll" => false);
        } else {
            $this->aNavParams = array("nPageSize" => $pageLimit,"bShowAll" => false);
        }
		return $this;
    }

    protected function prepareUserIds($ids){
        if (is_array($ids)){
            $ids = array_unique($ids);
            foreach ($ids as $key=>$val){
                $ids[ $key ] = intval($val);
            }

			$ids = implode('|', $ids);

        } else {
            $ids = intval($ids);
        }
        return $ids;
    }

	protected function prepareIds($ids){
		if (is_array($ids)){
			$ids = array_unique($ids);
			foreach ($ids as $key=>$val){
				$ids[ $key ] = intval($val);
			}
		} else {
			$ids = intval($ids);
		}
		return $ids;
	}

    public function getFilter(){
        return $this->aFilter;
    }

    public function getSelect(){
        return $this->aSelect;
    }

    public function getNavParams(){
        return $this->aNavParams;
    }

    public function getOrder(){
        return $this->aOrder;
    }
}