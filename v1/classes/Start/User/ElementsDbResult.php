<?php

namespace Start\User;

class ElementsDbResult {

	/* @var \CDBResult */
	protected $dbResult = null;

    protected $bTextHtmlAuto = true;

	public function __construct(\CDBResult $dbResult){
		$this->dbResult = $dbResult;
	}

    public function getCountItems(){
        return $this->dbResult->SelectedRowsCount();
    }

    public function getCountItemsOnPage(){
        return $this->dbResult->NavPageSize;
    }

    public function getNavCurrentPage(){
        return $this->dbResult->NavPageNomer;
    }

    public function getNavCountPages(){
        return $this->dbResult->NavPageCount;
    }

    public function getNavRecordCount(){
        return $this->dbResult->NavRecordCount;
    }

	public function setFetchParams($bTextHtmlAuto = true){
		$this->bTextHtmlAuto = $bTextHtmlAuto;
		return $this;
	}

	public function fetch(){
		if ($aItem = $this->dbResult->GetNext($this->bTextHtmlAuto)){
			$aItem = $this->prepareFetch($aItem);
		}
		return $aItem;
	}

	public function fetchAll($indexKey = false){
		$list = array();

		while ($aItem = $this->fetch()){
			if ($indexKey){
				$list[ $aItem[$indexKey] ] = $aItem;
			} else {
				$list[] = $aItem;
			}
		}

		return $list;
	}


    protected function prepareFetch($aItem){
        return $aItem;
    }
}
