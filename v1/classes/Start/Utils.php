<?php 

namespace Start;

class Utils
{

	public static function arrayFirst($input, $default = 0){
		return is_array($input) ? array_shift($input) : $default;
	}

    public static function arrayFirstKey($array){
        reset($array);
        return key($array);
    }

	public static function arrayColumn(array $input, $columnKey, $indexKey=false){
		$output = array();
		if ($indexKey){
			foreach ($input as $aItem){
                $output[ $aItem[$indexKey] ] = $aItem[$columnKey];
			}
		} else {
			foreach ($input as $aItem){
                $output[] = $aItem[$columnKey];
			}
		}

		return $output;
	}

    public static function arraySmartMerge($default, $fields){
        foreach ($default as $key => $val){
            if (isset($fields[$key])) {
                if (is_array($val) && is_array($fields[$key])){
                    $default[$key] = self::arraySmartMerge($val, $fields[$key]);
                } else {
                    $default[$key] = $fields[$key];
                }
            }
            unset($fields[$key]);
        }

        foreach ($fields as $key=>$val){
            $default[$key] = $val;
        }

        return $default;
    }

	public static function camelizeText($str, $prefix = ''){
        $str = str_replace(array('_', '-', ' '), '*', $str);
		$str = explode('*', $str);

		$tmp = !empty($prefix) ? array($prefix) : array();
		foreach ($str as $val){
			$tmp[] = ucfirst(strtolower($val));
		}

		return implode('', $tmp);
	}
	
	public static function getPluralWord($n, $form1, $form2, $form5) {
        $n = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20) return $form5;
        if ($n1 > 1 && $n1 < 5) return $form2;
        if ($n1 == 1) return $form1;
        return $form5;
    }
    
	public static function objectToArray ( $xmlObject, $xmlArray = array () ) {
		foreach ( (array) $xmlObject as $index => $node ){
			$xmlArray[$index] = ( is_object ( $node ) || is_array($node) ) ? self::objectToArray ( $node ) : $node;
		}
		return $xmlArray;
	}
	
	public static function getArrayValueByKeyPath($input, $path){
		$segments = is_array($path) ? $path : explode('.', $path);
		$cur =& $input;
		
		foreach ($segments as $segment){
			if (!isset($cur[$segment])){
				return null;
			}

			$cur = $cur[$segment];
		}

		return $cur;	
	}
	
	public static function setArrayValueByKeyPath(&$input, $value, $path) {
		$segments = is_array($path) ? $path : explode('.', $path);
		$cur =& $input;
		
		foreach ($segments as $segment){
			if (!isset($cur[$segment])){
				$cur[$segment] = array();
			}
			
			$cur =& $cur[$segment];
		}
		
		$cur = $value;
	}

    public static function arrayRows($input, $countCols = 3, $addEmpty=true){
		$rows = array();

		$rowIndex = 1;
		$colIndex = 1;

		foreach ($input as $aItem){

			if ($colIndex > $countCols){
				$colIndex = 1;
				$rowIndex++;
			}

			$rows['row'.$rowIndex][] = $aItem;
			$colIndex++;

		}

		if ($addEmpty){
			$diff = $countCols - count($rows['row'.$rowIndex]);
			if ($diff > 0){
				for ($index = 1;$index <= $diff;$index++){
					$rows['row'.$rowIndex][] = array();
				}
			}
		}

		return $rows;
	}


	public static function renderTemplate($templatePath, $vars = array()){
		if (is_array($vars)){
			extract($vars, EXTR_SKIP);
		}

		ob_start();

		include($templatePath);

		$html = ob_get_clean();

		return $html;
	}

	public static function exportToFile($filename, $data){
		$data = "<?php return " . var_export($data, 1) . ";";
		file_put_contents($filename, $data, LOCK_EX);
	}

	public static function importFromFile($filename){
		$data = null;
		if (file_exists($filename)){
			$data = include($filename);
		}

		return $data;
	}
}
