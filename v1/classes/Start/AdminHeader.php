<?php

namespace Start;

class AdminHeader
{

    protected $currentUri;
    protected $filesPath;

    public function __construct($filesPath) {
        $filesPath = rtrim($filesPath, '/') . '/';

        $this->currentUri = $this->getApplication()->GetCurUri();
        $this->filesPath = $filesPath;
    }

    public function execute($routes) {
        $files = $this->findCurrentFiles($routes);
        foreach ($files as $val) {
            if (file_exists($this->filesPath . $val . '.php')) {
                include $this->filesPath . $val . '.php';
            }
        }
    }

    protected function findCurrentFiles($routes) {
        $files = array();
        foreach ($routes as $file => $url) {

            $parts = parse_url($url);

            $list = array();
            $list[] = $parts['path'];

            if (!empty($parts['query'])) {
                $tmp = explode('&', $parts['query']);
                foreach ($tmp as $val) {
                    $list[] = $val;
                }
            }

            $cntAll = count($list);
            $cntMatches = 0;

            foreach ($list as $val) {
                if (false !== strpos($this->currentUri, $val)) {
                    $cntMatches++;
                }
            }

            if ($cntAll == $cntMatches) {
                $files[] = $file;
            }
        }

        return $files;
    }


    /* @return \CMain */
    protected function getApplication(){
        return $GLOBALS['APPLICATION'];
    }
}