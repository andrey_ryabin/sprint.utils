<?php

namespace Start;

class Component {

    public static function getUniqueId(\CBitrixComponent $component, $mergeParams = array()){
        $mergeParams = array_merge($component->arParams, $mergeParams);
		$mergeParams = self::removeRawParams($mergeParams);

		unset($mergeParams['is_backend']);

        $value = array(
            'name' => $component->getName(),
            'template' => $component->getTemplateName(),
            'params' => $mergeParams
        );

        $componentId = 'cmp' . md5(serialize($value));

        $file = self::getBackendFile($componentId);
        if (!file_exists($file)){
            $value = "<?php return " . var_export($value, 1) . ";";
            file_put_contents($file, $value, LOCK_EX);
        }

        return $componentId;
    }

    public static function executeFromRequest(){
        if (empty($_REQUEST['component'])){
            return false;
        }

        $componentId = $_REQUEST['component'];
        $componentId = str_replace(array("\r\n", "\n", "\r", '.', '\\', '/', ' '), '', trim($componentId));
        if (strlen($componentId) < 10){
            return false;
        }

        $file = self::getBackendFile($componentId);
        if (!file_exists($file)){
            return false;
        }

        $value = include($file);
        if (empty($value)){
            return false;
        }

        @touch($file);

		if (empty($value['params']) || !is_array($value['params'])){
			$value['params'] = array();
		}

		$value['params']['is_backend'] = 'ok';

        
        return Bitrix::getApplication()->IncludeComponent($value['name'], $value['template'], $value['params'], false);
    }

	public static function cleanOldFiles(){
		$dir = self::getCacheDir();
		$maxtime = time() - 24 * 3600;
		$it = new \RecursiveDirectoryIterator($dir);
		foreach(new \RecursiveIteratorIterator($it) as $file) {
			/* @var $file \SplFileInfo */

			$ext = pathinfo($file->getFilename(), PATHINFO_EXTENSION);

			if ($file->isFile() && $ext == 'php'){
				if ($file->getATime() < $maxtime){
					@unlink($file->getPathname());
				}
			}
		}

		return 'Start\Component::cleanOldFiles();';
	}

	public static function getCacheInfo(){
		$dir = self::getCacheDir();
		$out = array();

		$it = new \RecursiveDirectoryIterator($dir);
		foreach(new \RecursiveIteratorIterator($it) as $file) {
			/* @var $file \SplFileInfo */

			$ext = pathinfo($file->getFilename(), PATHINFO_EXTENSION);

			if ($file->isFile() && $ext == 'php'){
				$out[] = array(
					'file' => $file->getFilename(),
					'atime' => $file->getATime(),
					'size' => $file->getSize(),
				);
			}
		}

		return $out;
	}

	public static function getCacheDir(){
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/component_cache/';
		if (!is_dir($dir)){
			mkdir($dir, BX_DIR_PERMISSIONS, true);
		}
		return $dir;
	}

	public static function getCacheDirByComponent($componentId){
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/component_cache/' . substr($componentId,0, 6) . '/';
		if (!is_dir($dir)){
			mkdir($dir, BX_DIR_PERMISSIONS, true);
		}
		return $dir;
	}


	protected static function getBackendFile($componentId){
		$dir = self::getCacheDirByComponent($componentId);
		return $dir . '/' . $componentId . '.php';
	}

	protected function removeRawParams($params){
		$params2 =  array();
		foreach ($params as $key=>$item){
			if (0 !== strpos($key, '~')) {
				$params2[$key] = $item;
			}
		}
		return $params2;
	}


    public static function install(){
        if (!is_dir($_SERVER["DOCUMENT_ROOT"]."/bitrix/component_cache")){
            mkdir($_SERVER["DOCUMENT_ROOT"]."/bitrix/component_cache", BX_DIR_PERMISSIONS);
        }
        
        \CAgent::AddAgent('Start\\Component::cleanOldFiles();', 'start' , 'N', 86400);
    }

    public static function uninstall(){
        \CAgent::RemoveAgent('Start\\Component::cleanOldFiles();', 'start');
    }
}
