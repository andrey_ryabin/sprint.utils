<?php

namespace Start\Votes;

class SimpleVote {

	protected $aVoteFields = array();
	protected $aQuestions = array();

	public function __construct($title, $channelId, $dateStart = false, $dateEnd = false){
		\CModule::IncludeModule("vote");

		$uniqSession = 1;
		$uniqCookie = 2;
		$uniqIP = 0;//4;
		$uniqID = 0;//8;
		$uniqIDNew = 0;

		$dateStart = ($dateStart) ? $dateStart : date('d.m.Y H:i:s', time() - 3600 * 2);
		$dateEnd = ($dateEnd) ? $dateEnd : date('d.m.Y H:i:s', time() + 3600 * 24 * 30);

		$uniqType = $uniqSession | $uniqCookie | $uniqIP | $uniqID | $uniqIDNew;
		$uniqType += 5;

		$this->aVoteFields = array(
			"CHANNEL_ID" => $channelId,
			"C_SORT" => '300',
			"ACTIVE" => 'Y',
			"DATE_START" => $dateStart,
			"DATE_END" => $dateEnd,
			"TITLE" => $title,
			"DESCRIPTION" => '',
			"DESCRIPTION_TYPE" => 'html',
			"IMAGE_ID" => array(),
			"EVENT1" => 'vote',
			"EVENT2" => 'news_polls',
			"EVENT3" => '',
			"UNIQUE_TYPE" => $uniqType,
			"DELAY" => 10,
			"DELAY_TYPE" => 'M',
			"TEMPLATE" => '',
			"RESULT_TEMPLATE" => '',
			"NOTIFY" => 'N',
			"URL" => ''
		);
	}


	public function addQuestion($text, $answers = array()){
		$this->aQuestions[] = array('QUESTION' => $text, 'ANSWERS' => $answers);
	}

	protected function createAnswers($voteQuestionId, $sAnswerText){
		$aAnswerFields = array(
			"QUESTION_ID" => $voteQuestionId,
			"ACTIVE" => 'Y',
			"C_SORT" => 100,
			"MESSAGE" => $sAnswerText,
			"FIELD_TYPE" => 0, //0 radio, 1 checkbox, 2 dropdown, 3 multiselect, 4 text, 5 textarea
			"FIELD_WIDTH" => 0,
			"FIELD_HEIGHT" => 0,
			"FIELD_PARAM" => '',
			"COLOR" => ''
		);

		\CVoteAnswer::Add($aAnswerFields);
	}

	protected function createQuestions($voteId){
		foreach ($this->aQuestions as $aItem){

			$aQuestionsFields = array(
				"ACTIVE"		=> 'Y',
				"VOTE_ID"		=> $voteId,
				"C_SORT"		=> 100,
				"QUESTION"		=> $aItem['QUESTION'],
				"QUESTION_TYPE"	=> 'html',
				"IMAGE_ID"		=> array(),
				"DIAGRAM"		=> 'Y',
				"REQUIRED"		=> 'N',
				"DIAGRAM_TYPE"	=> 'histogram',
				"TEMPLATE"		=> '',
				"TEMPLATE_NEW"	=> ''
			);

			$voteQuestionId = \CVoteQuestion::Add($aQuestionsFields);

			if ($voteQuestionId > 0) {

				foreach ($aItem['ANSWERS'] as $sAnswerText) {
					$this->createAnswers($voteQuestionId, $sAnswerText);
				}

			}


		}
	}

	public function create(){
		$voteId = (int) \CVote::Add($this->aVoteFields);

		if ($voteId > 0){
			$this->createQuestions($voteId);
		}

		return $voteId;
	}
}