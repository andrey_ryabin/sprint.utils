<?php 

namespace Start;

class Debug
{

	public static function varDump($input){
        if (is_array($input)){
            echo '<pre>';print_r($input); echo '<pre/>';
        } else {
            echo '<pre>';var_dump($input); echo '<pre/>';
        }
	}


	public static function varDumpToLog($var, $fileName='logger'){
		if (!is_string($var) && !is_numeric($var)){
			ob_start(); is_array($var) ? print_r($var) : var_dump($var); $var = ob_get_clean();
		}

		$dir = self::getDebugDir();

		$file = $dir . $fileName.'.txt';

		$var = startf("[%s] %s \n", date('D M d H:i:s Y'), $var);

		$fp = fopen($file, 'a+');
		flock($fp, LOCK_EX);
		fwrite($fp, $var);
		flock($fp, LOCK_UN);
		fclose($fp);
	}

	public static function varDumpToFile($var, $fileName='vardump') {

		$dir = self::getDebugDir();
		$file = $dir. $fileName . date('Y-m-d-H-i-s').'.txt';

		ob_start();

		if (is_array($var)) {
			print_r($var);
		} else {
			var_dump($var);
		}

		$var = ob_get_clean();

		$fp = fopen($file, 'w+');
		flock($fp, LOCK_EX);
		fwrite($fp, $var);
		flock($fp, LOCK_UN);
		fclose($fp);
	}

	protected static function getDebugDir(){
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/__vardumps__/';
		if (!is_dir($dir)) {
			mkdir($dir, BX_DIR_PERMISSIONS);
		}
		return $dir;
	}
}
