<?php

namespace Start;

abstract class IblockElement {

	private $newFields = array();
	private $newProps = array();

    private $allProps = array();

	private $lastError = '';

    protected $iblockCode = '';
    private $iblockId = 0;

    
    protected function initialize() {
        //
    }

	public function __construct(){
        if (!$this->iblockCode){
            Throw new \Exception('Iblock not found');
        }

        $this->iblockId = Storage::getIblock()->getIdByCode($this->iblockCode);
        if (!$this->iblockId){
            Throw new \Exception('Iblock not found');
        }

        $all = Storage::getIblock()->getProperties($this->iblockCode, 'CODE');
        foreach ($all as $aProp){
            $this->allProps[ $aProp['CODE'] ] = $aProp;
        }

        $this->initialize();
	}

	public static function newInstance(){
		$class = get_called_class();
		return new $class;
	}


    public function update($id){
		if (!$element = $this->getElementById($id)){
            $this->lastError = 'Element not found';
            return false;
        }

        $aPropertyValues = array();
        if (!empty($this->newProps)){

            foreach ($this->allProps as $aProp){
                $type = $aProp['PROPERTY_TYPE'];
                $code = $aProp['CODE'];
                $mult = $aProp['MULTIPLE'];

                if (isset($this->newProps[$code])){
                    if ($type == 'F' && $mult == 'Y') {
                        $valueIds = $element['PROPERTY_'.$code.'_PROPERTY_VALUE_ID'];
                        $value = array();
                        foreach ($valueIds as $valId ){
                            $value[$valId] = array(
                                'VALUE' => $this->makeFileArrayToDel(),
                            );
                        }

                        foreach ($this->newProps[$code] as $newFileKey => $newFile){
                            $value['n' . $newFileKey] = $newFile;
                        }

                        $aPropertyValues[$code] = $value;


                    }elseif ($type == 'F' && $mult != 'Y') {
                        $valueId = $element['PROPERTY_'.$code.'_VALUE_ID'];
                        $value = $this->newProps[$code];
                        $aPropertyValues[$code] = array(
                            $valueId => $value
                        );
                    } else {
                        $value = $this->newProps[$code];
                        $aPropertyValues[$code] = $value;
                    }



                } elseif ($type != 'F') {

                    if ($type == 'L' && $mult == 'Y') {
                        $rawValue = $element['PROPERTY_' . $code . '_VALUE'];
                        $value = array();
                        foreach ($rawValue as $rawKey=> $val){
                            $value[] = array('VALUE' => $rawKey);
                        }

                        $aPropertyValues[$code] = $value;

                    } elseif ($type == 'L') {
                        $rawValue = $element['PROPERTY_' . $code . '_ENUM_ID'];
                        $value = array('VALUE' => $rawValue);
                        $aPropertyValues[$code] = $value;

                    }elseif ($mult == 'Y'){
                        $rawValue = $element['PROPERTY_' . $code . '_VALUE'];
                        $value = array();
                        foreach ($rawValue as $val){
                            $value[] = array('VALUE' => $val);
                        }

                        $aPropertyValues[$code] = $value;
                    } else {
                        $rawValue = $element['PROPERTY_' . $code . '_VALUE'];
                        $value = array('VALUE' => $rawValue);
                        $aPropertyValues[$code] = $value;
                    }


                }

            }
        }

        if (!empty($aPropertyValues)){
            $this->newFields['PROPERTY_VALUES'] = $aPropertyValues;
        }

        if (!empty($this->newFields)){
            $bUpdateSearch = true;
            $bResize = true;

            $el = new \CIBlockElement;
            $res = $el->Update($element['ID'], $this->newFields, false, $bUpdateSearch, $bResize);
            $this->lastError = $el->LAST_ERROR;
            return $res;
        }

        return false;
	}

    public function create(){
		$aFields = $this->newFields;
        $aProps = $this->newProps;

		$aFields['IBLOCK_ID'] = $this->iblockId;
		$aFields['PROPERTY_VALUES'] = $aProps;

        $bUpdateSearch = true;
        $bResize = true;

		$el = new \CIBlockElement;
		$res = $el->Add($aFields, false, $bUpdateSearch, $bResize);

		$this->lastError = $el->LAST_ERROR;
		return $res;
	}

    public function getLastError(){
        return strip_tags($this->lastError);
    }

    public function setField($code, $val){
        if (in_array($code, array('PREVIEW_PICTURE', 'DETAIL_PICTURE'))){
            if ($val){
                $val = $this->makeFileArray($val);
            } else {
                $val = $this->makeFileArrayToDel();
            }
        } elseif (in_array($code, array('DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO'))){
            //
        }

        $this->newFields[$code] = $val;
    }

    public function setProperty($code, $val, $descr =''){
        $prop = isset($this->allProps[$code]) ? $this->allProps[$code] : false;
        if (!$prop) return false;

        if ($prop['PROPERTY_TYPE'] == 'F'){
            if ($val){
                $val = $this->makeFileArray($val);
            } else {
                $val = $this->makeFileArrayToDel();
            }
        }

        if ($prop['MULTIPLE']=='Y'){
            $this->addNewPropValue($code, $val, $descr);
        } else {
            $this->setNewPropValue($code, $val, $descr);
        }
    }

    public function setPropertyByXmlId($code, $xmlId){
        $val = Storage::getIblock()->getEnumIdByXmlId($this->iblockCode, $code, $xmlId);
        $this->setProperty($code, $val);
        return $this;
    }

    public function setPropertyByValue($code, $value){
        $val = Storage::getIblock()->getEnumIdByValue($this->iblockCode, $code, $value);
        $this->setProperty($code, $val);
        return $this;
    }

    private function setNewPropValue($code, $val, $descr =''){
        $val = array('VALUE' => $val);
        if ($descr){
            $val['DESCRIPTION'] = $descr;
        }
        $this->newProps[$code] = $val;
    }

    private function addNewPropValue($code, $val, $descr =''){
        $val = array('VALUE' => $val);
        if ($descr){
            $val['DESCRIPTION'] = $descr;
        }
        if (empty($this->newProps[$code])){
            $this->newProps[$code] = array($val);
        } else {
            $this->newProps[$code][] = $val;
        }
    }


    protected function getElementById($id){
        $dbRes = \CIBlockElement::GetList(
            array('SORT' => 'ASC'),
            array('CHECK_PERMISSIONS' => 'N', 'ID' => intval($id), 'IBLOCK_ID' => $this->iblockId),
            false,
            false,
            $this->getIblockListSelect()
        );

        return $dbRes->Fetch();
    }


    protected function makeFileArray($val){
//        $req = array('name', 'type', 'tmp_name');
        if (!is_array($val)){
            $val = \CFile::MakeFileArray($val);
        }
        return $val;
    }

    protected function makeFileArrayToDel(){
        return array('name' => '','type' => '','tmp_name' => '','error' => 4,'size' => 0,'del' => 'Y');
    }


    protected function getIblockListSelect(){
        $list = array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'ACTIVE',
            'SHOW_COUNTER',
            'IBLOCK_SECTION_ID',
            'DATE_ACTIVE_FROM',
            'DATE_ACTIVE_TO',
            'DETAIL_PAGE_URL',
            'DATE_CREATE',
            'TIMESTAMP_X',
            'LANG_DIR'
        );
        foreach ($this->allProps as $aItem){
            $list[] = 'PROPERTY_' . $aItem['CODE'];
        }
        return $list;
    }

}