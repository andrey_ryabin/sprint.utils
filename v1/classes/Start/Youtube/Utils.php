<?php
namespace Start\Youtube;

class Utils {
 
    static public function getVideoCode($youtubeLink, $default=''){
        $matches = array();
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtubeLink, $matches)) {
            $videoCode = $matches[1];
        } else {
            $videoCode = $default;
        }
        return $videoCode;
    }
    
    static public function getPreviewImg( $videoUrl ){
    	$imageUrl	= 'http://img.youtube.com/vi/#video_code#/0.jpg';
    	return str_replace( '#video_code#', self::getVideoCode( $videoUrl ), $imageUrl );
    }
}