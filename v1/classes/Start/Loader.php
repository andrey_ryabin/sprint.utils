<?php

namespace Start;

class Loader
{
    private $includePath;
    private $includeNamespace;

    public function __construct($ns = null, $includePath = null) {
        $this->includeNamespace = $ns;
        $this->includePath = $includePath;
    }

    public function register() {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function unregister() {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    public function loadClass($className) {
        if (null === $this->includeNamespace || $this->includeNamespace . '\\' === substr($className, 0, strlen($this->includeNamespace . '\\'))) {
            $fileName = '';
            if (false !== ($lastNsPos = strripos($className, '\\'))) {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
            }
            $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

            $fileName = ($this->includePath !== null ? $this->includePath . DIRECTORY_SEPARATOR : '') . $fileName;
            if (is_readable($fileName)){
                require $fileName;
            }

        }
    }
}
