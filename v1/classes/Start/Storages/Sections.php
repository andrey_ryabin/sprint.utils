<?php
namespace Start\Storages;

class Sections extends Storage {

    protected function initialize(){
		//
    }

	public function getList($iIblockCode, $aFilter = array()){
		return $this->getFromCacheIblockSections($iIblockCode, $aFilter);
	}

    public function getTree($iIblockCode, $aFilter = array()){
        return $this->getFromCacheIblockSectionsTree($iIblockCode, $aFilter);
    }

	public function getById($iIblockCode, $sSectionId){
		$sSectionId = (int) $sSectionId;
		$list = $this->getList($iIblockCode);
        return array_key_exists($sSectionId, $list) ? $list[$sSectionId] : false;
	}

	public function getByCode($iIblockCode, $sSectionCode){
		$list = $this->getList($iIblockCode);
        foreach ($list as $aSection){
            if ($aSection['CODE'] == $sSectionCode){
                return $aSection;
            }
        }
		return false;
	}

	protected function getFromDbIblockSections($iblockId, $aFilter = array()){
        $ib = new \Start\Iblock\Sections();
        $ib->setIblockId($iblockId);
        $ib->mergeFilter($aFilter);
		return $ib->execute()->fetchAll('ID');
	}

    protected function getFromDbIblockSectionsTree($iblockId, $aFilter = array()){
        $ib = new \Start\Iblock\Sections();
        $ib->setIblockId($iblockId);
        $ib->mergeFilter($aFilter);
        $ib->setOrderByTree();
        return $ib->execute()->fetchAll('ID');
    }
}
