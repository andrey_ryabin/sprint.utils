<?php

namespace Start\Storages;

use \Start\Utils;

/**
 * Class Iblock
 * @package Start\Storages
 *
 * @method getFromCacheIblockList
 * @method getFromCacheIblockProperties
 * @method getFromCacheIblockEnumProperties
 *
 */
class Iblock extends Storage
{

    protected $aIblocks = array();

    protected function initialize() {
        $this->aIblocks = $this->getFromCacheIblockList();
    }

    public function getList() {
        return $this->aIblocks;
    }

    public function getListByType($type, $indexKey = 'CODE') {
        $list = array();
        foreach ($this->aIblocks as $aItem) {
            if ($aItem['IBLOCK_TYPE_ID'] == $type) {
                $list[$aItem[$indexKey]] = $aItem;
            }
        }

        return $list;
    }

    public function getByCode($iblockCode, $default = 0) {
        return array_key_exists($iblockCode, $this->aIblocks) ? $this->aIblocks[$iblockCode] : $default;
    }

    public function getById($iIblockId, $default = 0) {
        $res = $default;
        foreach ($this->aIblocks as $aItem) {
            if ($aItem['ID'] == $iIblockId) {
                $res = $aItem;
                break;
            }
        }
        return $res;
    }

    public function getIdByCode($sIblockCode, $default = 0) {
        $aItem = $this->getByCode($sIblockCode);
        return ($aItem && isset($aItem['ID'])) ? $aItem['ID'] : $default;
    }

    public function getCodeById($iIblockId, $default = 0) {
        $aItem = $this->getById($iIblockId);
        return ($aItem && isset($aItem['ID'])) ? $aItem['ID'] : $default;
    }

    public function getProperties($iIblockCode, $indexKey = 'CODE', $aSort = array("sort" => "asc")) {
        return $this->getFromCacheIblockProperties($iIblockCode, $indexKey, $aSort);
    }

    public function getPropertiesForSelect($iIblockCode) {
        $props = $this->getFromCacheIblockProperties($iIblockCode, 'CODE', array("sort" => "asc"));
        $porpsCodes = array_keys($props);
        $select = array();
        foreach ($porpsCodes as $val) {
            $select[] = 'PROPERTY_' . $val;
        }
        return $select;
    }


    public function getPropertyByCode($iIblockCode, $sPropertyCode) {
        $list = $this->getProperties($iIblockCode);
        return array_key_exists($sPropertyCode, $list) ? $list[$sPropertyCode] : false;
    }

    public function getPropertyIdByCode($iIblockCode, $sPropertyCode) {
        $aItem = $this->getPropertyByCode($iIblockCode, $sPropertyCode);
        return isset($aItem['ID']) ? $aItem['ID'] : false;
    }

    public function getEnumProperties($iIblockCode) {
        return $this->getFromCacheIblockEnumProperties($iIblockCode);
    }

    public function getEnumByCode($iIblockCode, $sPropertyCode) {
        $list = $this->getEnumProperties($iIblockCode);
        return array_key_exists($sPropertyCode, $list) ? $list[$sPropertyCode] : false;
    }

    public function getEnumIdByValue($iIblockCode, $sPropertyCode, $sValue) {
        $aItem = $this->getEnumByCode($iIblockCode, $sPropertyCode);
        $aItem = ($aItem) ? Utils::arrayColumn($aItem, 'VALUE', 'ID') : false;
        return ($aItem) ? array_search($sValue, $aItem) : false;
    }

    public function getEnumIdByXmlId($iIblockCode, $sPropertyCode, $sXmlId) {
        $aItem = $this->getEnumByCode($iIblockCode, $sPropertyCode);
        $aItem = ($aItem) ? Utils::arrayColumn($aItem, 'XML_ID', 'ID') : false;
        return ($aItem) ? array_search($sXmlId, $aItem) : false;
    }

    //
    protected function getFromDbIblockList() {
        $aList = array();
        $dbResult = \CIBlock::GetList(Array("SORT" => "ASC"), array("CHECK_PERMISSIONS" => "N"));
        while ($aItem = $dbResult->Fetch()) {
            $aList[$aItem['CODE']] = $aItem;
        }
        return $aList;
    }


    protected function getFromDbIblockProperties($iIblockCode, $indexKey = 'CODE', $aSort = array("sort" => "asc")) {
        $aList = array();
        $dbResult = \CIBlockProperty::GetList($aSort, array("IBLOCK_ID" => $this->getIdByCode($iIblockCode), "CHECK_PERMISSIONS" => "N"));
        while ($aItem = $dbResult->GetNext(true, false)) {
            $aList[$aItem[$indexKey]] = $aItem;
        }
        return $aList;
    }


    protected function getFromDbIblockEnumProperties($iIblockCode) {
        $aList = array();
        $dbResult = \CIBlockPropertyEnum::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $this->getIdByCode($iIblockCode)));
        while ($aItem = $dbResult->GetNext(true, false)) {
            $aList[$aItem['PROPERTY_CODE']][] = $aItem;
        }
        return $aList;
    }

}
