<?php
namespace Start\Storages;

/**
 * Class Languages
 * @package Start\Storages
 *
 * @method getFromCacheLanguages
 */
class Languages extends Storage {

	protected $langs = array();

    protected function initialize(){
		$this->langs = $this->getFromCacheLanguages();
    }

	public function getList(){
		return $this->langs;
	}

	public function getByLid($lid, $default = false){
		return isset($this->langs[$lid]) ? $this->langs[$lid] : $default;
	}


    protected function getFromDbLanguages(){
		$by = "sort";
		$order = "asc";
		$dbRes = \CLanguage::GetList($by, $order);

		$list = array();

		while ($aItem = $dbRes->Fetch()){
			$list[$aItem['LID']] = $aItem;
		}

		return $list;
    }
}
