<?php

namespace Start\Storages;

abstract class Storage
{

    private $phpCache;
    private $lifeTime;

    abstract protected function initialize();

    public function __construct() {
        $this->phpCache = new \CPHPCache;
        $this->lifeTime = 3600;

        $this->initialize();
    }

    public function __call($callback, $arguments) {
        if (false === strpos($callback, 'getFromCache')) {
            Throw new \Exception('getFromCacheUnknown');
        }

        $callback = str_replace('getFromCache', 'getFromDb', $callback);

        $initdir = $this->getCacheDirName();
        $uniqstr = md5($initdir . $callback . serialize($arguments));

        $mValue = array();

        if ($this->phpCache->InitCache($this->lifeTime, $uniqstr, $initdir)) {
            $mValue = $this->phpCache->GetVars();


        } elseif ($this->phpCache->StartDataCache($this->lifeTime, $uniqstr, $initdir)) {
            $mValue = call_user_func_array(array($this, $callback), $arguments);
            if (!$this->isEmpty($mValue)) {
                $this->phpCache->EndDataCache($mValue);
            }

        }

        $this->lifeTime = 3600;
        return $mValue;
    }


    protected function getCacheDirName() {
        $class = get_class($this);
        $class = str_replace(array('\\', '/', '_'), '', $class);
        return strtolower('start_cache_' . $class);
    }

    public function clear() {
        $initdir = $this->getCacheDirName();
        $this->phpCache->CleanDir($initdir);
    }

    protected function setLifeTime($lifetime) {
        $this->lifeTime = (int)$lifetime;
    }

    protected function isEmpty($var) {
        return in_array($var, array(null, '', array(), false), true);
    }
}
