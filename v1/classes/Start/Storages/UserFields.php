<?php
namespace Start\Storages;
use \Start\Utils;


class UserFields  extends Storage {

    protected function initialize(){

    }

	public function getEnumByName($sEntityId, $sFieldName){
		$iFieldId = $this->getFieldIdByName($sEntityId, $sFieldName);
		return ($iFieldId) ? $this->getFromCacheEnumValues($iFieldId) : false;
	}

	public function getEnumValues($sEntityId, $sFieldName){
		$aItem = $this->getEnumByName($sEntityId, $sFieldName);
		return ($aItem) ? Utils::arrayColumn($aItem, 'VALUE', 'ID') : false;
	}

	public function getEnumIdByValue($sEntityId, $sFieldName, $sValue){
		$aItem = $this->getEnumValues($sEntityId, $sFieldName);
		return ($aItem) ? array_search($sValue, $aItem) : false;

	}

	public function getFieldByName($sEntityId, $sFieldName){
		$list = $this->getFromCacheUserFields($sEntityId);
		return array_key_exists($sFieldName, $list) ? $list[ $sFieldName ] : false;
	}

	public function getFieldIdByName($sEntityId, $sFieldName){
		$aItem = $this->getFieldByName($sEntityId, $sFieldName);
		return isset($aItem['ID']) ? $aItem['ID'] : false;
	}

	protected function getFromDbEnumValues($iFieldId){
		$aList = array();

		$dbResult = \CUserFieldEnum::GetList(array('SORT' => 'ASC'), array('USER_FIELD_ID' => $iFieldId));
		while($aItem = $dbResult->Fetch()){
			$aList[ $aItem['ID'] ] = $aItem;
		}
		return $aList;
	}

    protected function getFromDbUserFields($sEntityId){
        $aList = array();

		$dbResult = \CUserTypeEntity::GetList( array('ID'=>'ASC', 'ENTITY_ID' => $sEntityId), array());
		while($aItem = $dbResult->Fetch()){
			$aList[ $aItem['FIELD_NAME'] ] = $aItem;
		}
        return $aList;
    }

}
