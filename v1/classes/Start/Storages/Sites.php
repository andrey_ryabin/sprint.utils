<?php
namespace Start\Storages;

/**
 * Class Languages
 * @package Start\Storages
 *
 * @method getFromCacheSites
 */
class Sites extends Storage {

	protected $langs = array();

    protected function initialize(){
		$this->langs = $this->getFromCacheSites();
    }

	public function getList(){
		return $this->langs;
	}

	public function getByLid($lid, $default = false){
		return isset($this->langs[$lid]) ? $this->langs[$lid] : $default;
	}


    protected function getFromDbSites(){
		$by = "sort";
		$order = "asc";
		$dbRes = \CSite::GetList($by, $order);

		$list = array();

		while ($aItem = $dbRes->Fetch()){
			$list[$aItem['LID']] = $aItem;
		}

		return $list;
    }
}
