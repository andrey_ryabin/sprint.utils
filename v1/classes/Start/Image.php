<?php

namespace Start;

class Image {

	static public function resizeImageById($iImageId, $width=0, $height=0, $exact=0){
		if ($aImage = self::getImageById($iImageId)){
			$aImage = self::resizeImage($aImage, $width, $height, $exact);
		}
		return $aImage;
	}


	static public function resizeImageByPath($path, $width=0, $height=0, $exact=0){
		if ($aImage = self::getImageByPath($path)){
			$aImage = self::resizeImage($aImage, $width, $height, $exact);
		}
		return $aImage;
	}


	static public function getImageById($id){
		return \CFile::GetFileArray($id);
	}
	
	static public function getImageByPath($path){
		$aImage = \CFile::MakeFileArray($path);
		if (empty($aImage)) return null;

		$docRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
		list($imWidth, $imHeight) = getimagesize($aImage['tmp_name']);

		$aImage['FILE_NAME'] = $aImage['name'];
		$aImage['WIDTH'] = $imWidth;
		$aImage['HEIGHT'] = $imHeight;

		$imageSrc = str_replace($docRoot, '', $aImage['tmp_name']);
		$imageDir = pathinfo($imageSrc, PATHINFO_DIRNAME);

		if ($imageDir == '/upload' || 0 !== strpos($imageDir, '/upload')){

			if ($imageDir == '/upload'){
				$imageDir = '/upload/external';
			} elseif ($imageDir == '/') {
				$imageDir = '/upload/external/index';
			} else {
				$imageDir = '/upload/external' . $imageDir;
			}

			if (!is_dir($docRoot . $imageDir)){
				mkdir($docRoot . $imageDir, BX_DIR_PERMISSIONS, true);
			}
			copy($aImage['tmp_name'], $docRoot . $imageDir . '/' .$aImage['name']);
		}

		$aImage['SUBDIR'] = str_replace('/upload/', '', $imageDir);
		$aImage['SRC'] = $imageDir . '/' . $aImage['name'];
		return $aImage;
	}

	protected function resizeImage($aImage, $width=0, $height=0, $exact=0){
		if ($width > 0 && $height > 0) {
			$mode = ($exact) ? BX_RESIZE_IMAGE_EXACT : BX_RESIZE_IMAGE_PROPORTIONAL;
			$aFileTmp = \CFile::ResizeImageGet($aImage, array("width" => $width, "height" => $height),$mode,true);
			return array("ORIGIN_SRC" => $aImage["SRC"], "ID" => $aImage['ID'], "SRC" => $aFileTmp["src"], "WIDTH" => $aFileTmp["width"],"HEIGHT" => $aFileTmp["height"]);
		} else {
			return $aImage;
		}
	}
	
}