<?php

namespace Start\Yandex;

class Referats {

	protected $category = array(
		'astronomy',
		'geology',
		'gyroscope',
		'literature',
		'marketing',
		'mathematics',
		'music',
		'polit',
		'agrobiologia',
		'law',
		'psychology',
		'geography',
		'physics',
		'philosophy',
		'chemistry',
		'estetica',
	);

	protected $query = array();

	public function __construct($category = false){
		if (!empty($category)){
			if (is_array($category)){
				foreach ($category as $val){
					if (in_array($val, $this->category)){
						$search[] = $val;
					}
				}
			}elseif (in_array($category, $this->category)){
				$search[] = $category;
			}
		}
		if (empty($search)){
			$search = $this->category;
		}

		$this->query = array(
			'mix' => implode(',', $search)
		);

		foreach ($search as $val){
			$this->query[$val] = 'on';
		}
	}

	public function fetchAll($limit, $segment=false){
		$limit = (int) $limit;
		$limit = ($limit >= 1) ? $limit : 1;

		$result = array();
		for ($index = 0;$index < $limit;$index++){
			if ($aItem = $this->fetch($segment)){
				$result[] = $aItem;
			}

		}

		return $result;
	}

	public function fetch($segment=false){
		$query = 'http://referats.yandex.ru/all.xml?' . http_build_query($this->query);
		$str = file_get_contents($query);

		if (preg_match('/class="text">(.*)<\/td>/iUs', $str, $matches1)){
			if (preg_match('/<h2>(.*)<\/h2>.*<h1.*>(.*)<\/h1>(.*)/is', $matches1[1], $matches2)){
				$content = array(
					'subject' => $matches2[1],
					'title' => $matches2[2],
					'text' => $matches2[3],
				);

                return ($segment && isset($content[$segment])) ? $content[$segment] : $content;
			}
		}

		return false;
	}

}