<?php

namespace Start\Yandex;

class Images {

	protected $textToFind = '';
	
	protected $offset = 0;
	protected $page = 1;

	protected $connect = 0;

	protected $minWidth = 1024;
	protected $minHeight = 768;

	protected $maxWidth = 0;
	protected $maxHeight = 0;
	
	protected $result = array();
	
	public function __construct($textToFind, $page = 1){
		$this->textToFind = $textToFind;
		$this->setPage($page);
	}

	public function setMaxSize($width=0, $height=0){
		$this->maxWidth = $width;
		$this->maxHeight = $height;
	}

	public function setMinSize($width=0, $height=0){
		$this->minWidth = $width;
		$this->minHeight = $height;
	}

	public function setPage($page = 1){
		$page = (int) $page;
		$page = ($page >= 1) ? $page : 1;
		$this->page = $page;
	}

	public function fetchAll($limit, $segment=false){
		$limit = (int) $limit;
		$limit = ($limit >= 1) ? $limit : 1;

		$result = array();
		for ($index = 0;$index < $limit;$index++){
			$result[] = $this->fetch($segment);
		}

		return $result;
	}

	public function fetch($segment=false){
		while(!isset($this->result[$this->offset])){

			$this->addResult($this->textToFind, $this->page);

			$this->connect++;
			$this->page++;
		}

		$image = $this->result[$this->offset];
		$this->offset++;
		return ($segment && isset($image[$segment])) ? $image[$segment] : $image;
	}


	public function getStatus(){
		return array(
			'requests' => $this->connect,
			'offset' => $this->offset,
			'page' => $this->page,
		);
	}
	
	protected function addResult($find, $page){
		$q = array (
			'text' => $find,
			'p' => $page,
			'format' => 'json',
			'request' => '[{"block":"serp-controller","params":{}},{"block":"serp-list_infinite_yes","params":{}},{"block":"more_direction_next","params":{}},{"block":"ban-message","params":{}}]',
			'isize' => 'large',
			'iorient' => 'horizontal',
			'type' => 'photo',
			'uinfo' => 'sw-1920-sh-1080-ww-1905-wh-477-pd-1-wp-16x9_1920x1080',
			'rpt' => 'image',
			'serpid' => '',
		);

		$json = file_get_contents('https://yandex.com/images/search?' . http_build_query($q));
		$json = json_decode($json, 1);


		if (empty($json['serp-list_infinite_yes']) || empty($json['serp-list_infinite_yes']['html'])){
			return false;
		}

		$html = $json['serp-list_infinite_yes']['html'];

		preg_match_all('/onclick="return (.*)"/iUs', $html, $matches, PREG_SET_ORDER);

		if (empty($matches)){
			return false;
		}

		foreach ($matches as $match){
			$json = htmlspecialchars_decode($match[1], ENT_QUOTES);
			$json = json_decode($json, 1);

			if (empty($json['serp-item'])){
				continue;
			}

			$images = array();

			foreach (array('preview', 'fullscreen', 'dups') as $type){
				if (empty($json['serp-item'][$type])){
					continue;
				}

				$items = $json['serp-item'][$type];

				foreach ($items as $img){
					
					$ok = true;
					if ($this->minWidth > 0 && $img['width'] < $this->minWidth){
						$ok = false;	
					}
					if ($this->minHeight > 0 && $img['height'] < $this->minHeight){
						$ok = false;
					}

					if ($this->maxWidth > 0 && $img['width'] > $this->maxWidth){
						$ok = false;
					}
					if ($this->maxHeight > 0 && $img['height'] > $this->maxHeight){
						$ok = false;
					}					
					
					if ($ok){
						$images[] = $img;
					}
					
				}
			}



			if (!empty($images)){

				$maxW = 0;
				$maxK = 0;

				foreach ($images as $key => $img){
					if ($img['width'] > $maxW){
						$maxK = $key;
						$maxW = $img['width'];
					}
				}

				$maxImage = $images[$maxK];

				unset($images[$maxK]);
				$maxImage['other'] = $images;

				$this->result[] = $maxImage;
			}

		}

		return true;
	}

}