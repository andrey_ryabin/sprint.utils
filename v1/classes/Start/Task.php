<?php

namespace Start;

abstract class Task {

    protected $colors = array();

    protected $outmode = 'console';

	public function __construct(){
        $this->initializeColors();
        $this->initialize();
	}

    public function executeHello(){
        $this->out('[light_blue]Hello![/]');
    }

    protected function initialize(){
        //
    }

    public function setOutMode($mode){
        $this->outmode = $mode;
    }

    private function outToConsole($msg){
        foreach ($this->colors as $key => $val) {
            $msg = str_replace('[' . $key . ']', "\033[" . $val . "m", $msg);
        }

        $msg = str_replace('[/]', "\033[0m", $msg);

        echo $msg . "\n";
    }


    private function outToLog($msg){

    }

    private function outToHtml(){

    }

    protected function out($msg, $var1 = null, $var2 = null) {
        ob_end_flush();

        if (func_num_args() > 1) {
            $params = func_get_args();
            $msg = call_user_func_array('startf', $params);
        }

        if ($this->outmode == 'log'){
            $this->outToLog($msg);
        } else {
            $this->outToConsole($msg);
        }


        @ob_flush();
    }


    private function initializeColors() {
        //text
        $this->colors['black'] = '0;30';
        $this->colors['dark_gray'] = '1;30';
        $this->colors['blue'] = '0;34';
        $this->colors['light_blue'] = '1;34';
        $this->colors['green'] = '0;32';
        $this->colors['light_green'] = '1;32';
        $this->colors['cyan'] = '0;36';
        $this->colors['light_cyan'] = '1;36';
        $this->colors['red'] = '0;31';
        $this->colors['light_red'] = '1;31';
        $this->colors['purple'] = '0;35';
        $this->colors['light_purple'] = '1;35';
        $this->colors['brown'] = '0;33';
        $this->colors['yellow'] = '1;33';
        $this->colors['light_gray'] = '0;37';
        $this->colors['white'] = '1;37';

        //background
        $this->colors['bg_black'] = '40';
        $this->colors['bg_red'] = '41';
        $this->colors['bg_green'] = '42';
        $this->colors['bg_yellow'] = '43';
        $this->colors['bg_blue'] = '44';
        $this->colors['bg_magenta'] = '45';
        $this->colors['bg_cyan'] = '46';
        $this->colors['bg_light_gray'] = '47';
    }
}
