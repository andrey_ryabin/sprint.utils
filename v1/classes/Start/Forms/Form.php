<?php

namespace Start\Forms;
use Start\Forms\Field;
use Start\Forms\Fields\Hidden;

class Form {

    /** @var array|Field[] */
    private $fields = array();

    private $form_name = '';

    private $templatePath = '';
    private $templateDefault = '';

    private $errors = array();

    protected $params = array();

	public function __construct($name = 'form', $params = array()){
        $this->params = $params;
        $this->setTemplatePath('');
        $this->setName($name);
		$this->initialize();
	}

    public function setTemplatePath($path){
        $this->templateDefault = __DIR__ . '/templates/';
        if (!empty($path) && is_dir($path)){
            $this->templatePath = $path;
        }
    }

    public function getParam($name, $default = ''){
        return array_key_exists($name, $this->params) ? $this->params[$name] : $default;
    }

    public function setName($name = ''){
        $this->form_name = $name;
        $this->addField('form_name', new Hidden(array('value' => $name)));
    }

    protected function initialize() {
        //
    }

    /** @return array|Field[] */
    public function getFields(){
        return $this->fields;
    }

    /** @return Field */
    public function getField($name){
        return $this->fields[$name];
    }

    public function addField($name, Field $field){
        $field->initializeField($this->form_name . '_' . $name);
        $this->fields[$name] = $field;
    }

    public function addValidator($name, Validator $validator){
        $this->fields[$name]->addValidator($validator);
    }

    public function getValue($name){
        return $this->getField($name)->getValue();
    }

    public function submit(){
        if (empty($_POST[$this->form_name . '_form_name'])){
            return false;
        }
    
        if ($this->form_name != $_POST[$this->form_name . '_form_name']){
            return false;        
        }
    
        $ok = true;

        foreach ($this->fields as $name => $field){
            if (isset($_POST[$this->form_name . '_' . $name])){
                $field->setValue($_POST[$this->form_name . '_' . $name]);
            } else {
                $field->setValue('');
            }

            if (!$field->isValid()){
                $this->collectErrors($name, $field);
                $ok = false;
            }
        }

        return $ok;
    }

    public function isPost(){
        return ($_SERVER["REQUEST_METHOD"] == "POST");
    }

    public function getErrors(){
        return $this->errors;
    }

    private function collectErrors($name, Field $field){
        $err = $field->getErrors();
        foreach ($err as $e){
            $this->errors[$name][] = $e;
        }
    }

    private function render(){
        $res = '';
        $res .= $this->renderFile($this->getFileFromTemplate('form_header'), array(
            'enctype' => 'multipart/form-data',
            'method' => 'post',
            'form_name' => $this->form_name,
            'action' => '',
            'params' => $this->params,
        ));

        foreach ($this->fields as $name => $field){
            $str = $this->renderField($name, $field);
            $res .= $str;
        }

        $res .= $this->renderFile($this->getFileFromTemplate('form_footer'), array(
            'form_name' => $this->form_name,
            'params' => $this->params,            
        ));

        return $res;
    }


    private function getFileFromTemplate($template){
        if (is_file($this->templatePath . $template . '.php')){
            $file = $this->templatePath . $template . '.php';
        } else {
            $file = $this->templateDefault . $template . '.php';
        }
        return $file;
    }

    private function renderField($name, Field $field){
        $vars = $field->getTemplateVars();
        if (false === $vars){
            return '';
        }

        $vars['title'] = !empty($vars['title']) ? $vars['title'] : $name;

        $vars['id'] = $this->form_name . '_' . $name;
        $vars['form_name'] = $this->form_name;

        $vars['note'] = !empty($vars['note']) ? $vars['note'] : '';
        $vars['value'] = $field->getValue();
        $vars['errors'] = $field->getErrors();
        $vars['is_required'] = $field->getRequired();

        $template = $field->getTemplateName();
        $file = $this->getFileFromTemplate($template);
        return $this->renderFile($file, $vars);

    }

    private function renderFile($renderFile, $renderParams){
        /* @global $APPLICATION \CMain */
        /* @global $USER \CUser */

        if (is_array($renderParams)){
            extract($renderParams, EXTR_SKIP);
        }

        ob_start();

        global $APPLICATION;
        global $USER;

        include($renderFile);

        $html = ob_get_clean();

        return $html;
    }


    public function __toString(){
        return $this->render();
    }
}

