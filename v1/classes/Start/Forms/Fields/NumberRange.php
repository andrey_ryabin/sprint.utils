<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class NumberRange extends Field{

    protected function initialize(){
        $this->setTemplate('number_range');
    }


    protected function bindValue($value){
        $value = is_array($value) ? $value : array($value);

		$value['start'] = !empty($value['start']) ? abs(intval($value['start'])) : '';
		$value['end'] = !empty($value['end']) ? abs(intval($value['end'])) : '';

		return $value;
	}

}

