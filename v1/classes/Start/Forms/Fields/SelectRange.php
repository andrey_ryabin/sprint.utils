<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class SelectRange extends Field{

    protected function initialize(){
        $this->setTemplate('select_range',array('items'));
    }

	protected function bindValue($value){
        $value = is_array($value) ? $value : array($value);

		$items = $this->getParam('items', array());

		$value['start'] = (isset($value['start']) && array_key_exists($value['start'], $items)) ? $value['start'] : '';
		$value['end'] = (isset($value['end']) && array_key_exists($value['end'], $items)) ? $value['end'] : '';

		return $value;
	}

}

