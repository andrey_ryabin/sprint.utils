<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class ConfirmTerms extends Field{

    protected function initialize(){
        $this->setTemplate('confirm_terms', array('message'));
    }

	protected function bindValue($value){
		return $value;
	}

}

