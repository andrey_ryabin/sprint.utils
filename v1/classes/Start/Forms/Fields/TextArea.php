<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class TextArea extends Field{

    protected function initialize(){
        $this->setTemplate('textarea');
    }

	protected function bindValue($value){
		return $value;
	}
}

