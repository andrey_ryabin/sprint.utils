<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class Checkbox extends Field{

    protected function initialize(){
        $this->setTemplate('checkbox', array('items'));
    }

	protected function bindValue($value){
		$value = is_array($value) ? $value : array($value);

		$items = $this->getParam('items', array());
		$result = array();

		foreach ($value as $val){
			if (array_key_exists($val, $items)){
				$result[] = $val;
			}
		}

		$result = array_unique($result);
		return $result;
	}

}

