<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class Image extends Field{

    protected function initialize(){
        $this->setTemplate('image');
    }

	protected function bindValue($value){
	    $value = !is_array($value) ? array('id' => $value) : $value;
	
        $name = $this->getParam('name');
        if (isset($_REQUEST[$name .'_new'])){
            $value['id'] = $_REQUEST[$name .'_new'];
            $value['is_new'] = 'Y';
        }

        $value['del'] = (isset($value['del']) && $value['del'] == 'Y') ? 'Y' : 'N';
        if ($value['del'] == 'Y'){
            $value['id'] = '';
        }


		return $value;	
	}
}

