<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class Text extends Field{

    protected function initialize(){
        $this->setTemplate('text');
    }

	protected function bindValue($value){
		return $value;
	}
}

