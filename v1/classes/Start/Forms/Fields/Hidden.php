<?php

namespace Start\Forms\Fields;

use Start\Forms\Field;

class Hidden extends Field{

    protected function initialize(){
        $this->setTemplate('hidden');
    }

	protected function bindValue($value){
		return $value;
	}

}

