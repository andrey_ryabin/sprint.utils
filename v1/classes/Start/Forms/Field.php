<?php

namespace Start\Forms;


abstract class Field {

    private $params = array();
    private $value = '';
    private $errors = array();
    private $required = false;

    /** @var array|Validator[] */
    private $validators = array();


    private $template = 'field';
    private $required_vars =  array();

    abstract protected function bindValue($value);
    abstract protected function initialize();

    public function __construct($params = array()){
        $this->params = $params;
	}

    public function initializeField($name){
        $this->params['name'] = $name;
    
        $this->initialize();
            
       if (isset($this->params['value'])){
            $this->setValue($this->params['value']);
        } else {
            $this->setValue($this->value);
        }

        if (isset($this->params['template'])){
            $req = isset($this->params['required_vars']) ? $this->params['required_vars'] : array();
            $this->setTemplate($this->params['template'], $req);
        }

        if (isset($this->params['is_required'])){
            $this->setRequired($this->params['is_required']);
        } 
    }


    public function setRequired($required = false){
        $this->required = (bool) $required;
    }

    public function getRequired(){
        return $this->required;
    }
    
    protected function addError($err){
        $this->errors[] = $err;
    }

    public function setValue($value){
        $this->value = $this->bindValue($value);
    }

    public function getValue(){
        return $this->value;
    }

    public function isValid(){
        $value = $this->getValue();

        foreach ($this->validators as $validator){
            if (!$validator->isValid($value)){
                $this->addError($validator->getError());
                return false;
            }
        }

        return true;
    }

    public function setTemplate($name, $requiredParams = array()){
        $this->template = $name;
        $this->required_vars = $requiredParams;
    }

    public function getTemplateName(){
        return $this->template;
    }

    public function getTemplateVars(){
        foreach ($this->required_vars as $val){
            if (!array_key_exists($val, $this->params)){
                return false;
            }
        }
        return $this->params;
    }


    /* @return array */
    public function getErrors(){
        return $this->errors;
    }

    protected function getParam($name, $default = ''){
        return array_key_exists($name, $this->params) ? $this->params[$name] : $default;
    }

    protected function setParam($name, $val){
        $this->params[$name] = $val;
    }

    public function addValidator(Validator $validator){
        $this->validators[] = $validator;
    }

}

