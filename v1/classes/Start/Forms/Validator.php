<?php

namespace Start\Forms;


abstract class Validator {

    protected $error = 'error';

    abstract public function isValid($value);

    public function __construct($error = ''){
        $this->initialize();
        if (!empty($error)){
            $this->error = $error;
        }
    }

    public function getError(){
        return $this->error;
    }

    protected function initialize(){
        //
    }

    protected function isEmpty($value){
        return in_array($value, array(null, '', array(), false), true);
    }

}

