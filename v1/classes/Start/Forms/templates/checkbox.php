<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <?foreach ($items as $key=>$val):?>
            <input <?if (in_array($key, $value)):?>checked="checked"<?endif?> type="checkbox" id="<?=$id?>_<?=$key?>" name="<?=$name?>[]" value="<?=$key?>" >
            <label for="<?=$id?>_<?=$key?>" ><?=$val?></label>
        <?endforeach;?>

        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>