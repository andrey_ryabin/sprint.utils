<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <input  value="<?=$value['start']?>" name="<?=$name?>[start]" id="<?=$id?>_start" type="text" />
        &mdash;
        <input  value="<?=$value['end']?>" name="<?=$name?>[end]" id="<?=$id?>_end" type="text" />

        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>