<tr>
    <td>
        <?=$title?>
        <?if ($is_required):?><small >*</small><?endif?>
    </td>
    <td>
        <input  name="<?=$name?>" id="<?=$id?>" type="text" value="<?=$value?>" />
        <?if (!empty($errors)):?><div ><?=implode(', ', $errors)?></div><?endif?>
        <?if (!empty($note)):?><div ><?=$note?></div><?endif?>
    </td>
</tr>