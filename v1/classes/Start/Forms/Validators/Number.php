<?php

namespace Start\Forms\Validators;
use Start\Forms\Validator;

class Number extends Validator {

    public function isValid($value){

        if ($this->isEmpty($value) || is_numeric($value)){
            return true;
        }

        return false;
    }
}

