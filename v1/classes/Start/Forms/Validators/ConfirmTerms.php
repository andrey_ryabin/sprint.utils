<?php

namespace Start\Forms\Validators;
use Start\Forms\Validator;

class ConfirmTerms extends Validator {

    public function isValid($value){
		return ($value == 'Y');
    }
}

