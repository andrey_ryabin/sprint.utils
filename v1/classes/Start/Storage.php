<?php

namespace Start;

/**
 * Class Storage
 * @method static Storages\Iblock getIblock()
 * @method static Storages\Languages getLanguages()
 * @method static Storages\Sections getSections()
 * @method static Storages\UserFields getUserFields()
 * @method static Storages\Sites getSites()
 *
 */
class Storage
{

    private static $list = array();

    protected static $namespaces = array(
        'Start\\Storages'
    );

    public function __callStatic($name, $arguments) {
        $class = get_called_class();
        $namesp = $class::$namespaces;

        if (false === strpos($name, 'get')) {
            return null;
        }

        $name = substr($name, 3);
        if (isset(self::$list[$name])) {
            return self::$list[$name];
        }

        foreach ($namesp as $ns) {
            $class = $ns . '\\' . $name;
            if (class_exists($class)) {
                self::$list[$name] = new $class;
                return self::$list[$name];
            }
        }

        return null;
    }

    public static function showLoaded() {
        return array_keys(self::$list);
    }
}