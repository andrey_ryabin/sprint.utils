<?php

namespace Start;

class Assets {

    public static function includeAssets($webDir = '/assets/'){
        $rootDir = $_SERVER['DOCUMENT_ROOT'] . $webDir;
		$it = new \DirectoryIterator($rootDir);
        
        foreach($it as $file) {
            /* @var $file \SplFileInfo */
            if ($file->isFile()){
				$ext = pathinfo($file->getFilename(), PATHINFO_EXTENSION);

                if ($ext == 'js'){
                    Bitrix::getApplication()->AddHeadScript($webDir . $file->getFilename());
                    
                }elseif ($ext == 'css'){
                    Bitrix::getApplication()->SetAdditionalCSS($webDir . $file->getFilename());
                    
                }
            }
        }
    }



}
