<?php

namespace Start\Iblock;

class Sections
{

    protected $aOrder = array(
        'SORT' => 'ASC'
    );
    protected $aFilter = array(
        'CHECK_PERMISSIONS' => 'N'
    );
    protected $aGroupBy = false;
    protected $aSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'CODE',
        'SECTION_PAGE_URL',
        'PICTURE',
        'DEPTH_LEVEL',
        'IBLOCK_SECTION_ID'
    );

    protected $aNavParams = false;

    protected $bIncCnt = false;

    protected $dbResult = null;

    /* @return Sections */
    public static function create() {
        $class = get_called_class();
        return new $class;
    }

    /* @return \CDBResult */
    public function getDbResult() {
        return \CIBlockSection::GetList($this->aOrder, $this->aFilter, $this->bIncCnt, $this->aSelect, $this->aNavParams);
    }

    /* @return SectionsDbResult */
    public function execute() {
        if (empty($this->dbResult)) {
            $class = get_class($this) . 'DbResult';
            $class = class_exists($class, true) ? $class : 'Start\\Iblock\\SectionsDbResult';
            $ibRes = $this->getDbResult();
            $this->dbResult = new $class($ibRes);
        }
        return $this->dbResult;
    }

    public function setIblockId($id) {
        $this->aFilter['IBLOCK_ID'] = intval($id);
        return $this;
    }

    public function setActive($active = 'Y') {
        $active = ($active && $active == 'N') ? 'N' : 'Y';
        $this->aFilter['ACTIVE'] = $active;
        return $this;
    }

    public function mergeFilter($aFilter) {
        $this->aFilter = array_merge($this->aFilter, $aFilter);
        return $this;
    }

    public function mergeSelect($aSelect) {
        $this->aSelect = array_merge($this->aSelect, $aSelect);
        return $this;
    }

    public function setOrderByTree() {
        $this->aOrder = array("left_margin" => "asc");
        return $this;
    }

    public function setOrder($aOrder) {
        $this->aOrder = $aOrder;
        return $this;
    }

    public function setSectionId($ids) {
        $this->aFilter['SECTION_ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function retrieveCountElements($activeOnly = 'Y') {
        $this->bIncCnt = true;
        $activeOnly = ($activeOnly && $activeOnly == 'Y') ? 'Y' : 'N';
        $this->mergeFilter(array('CNT_ACTIVE' => $activeOnly, 'ELEMENT_SUBSECTIONS' => 'Y'));
        return $this;
    }

    public function setDepthLevel($level) {
        $level = (int)$level;
        $level = ($level >= 1) ? $level : 1;
        $this->aFilter['DEPTH_LEVEL'] = $level;
        return $this;
    }

    public function setExcludeIds($ids) {
        $this->aFilter['!ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setIds($ids) {
        $this->aFilter['ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setCode($code) {
        $this->aFilter['=CODE'] = $code;
        return $this;
    }

    public function setNavTopCount($topLimit = false) {
        if ($topLimit) {
            $this->aNavParams = array('nTopCount' => $topLimit);
        } else {
            $this->aNavParams = false;
        }
        return $this;
    }

    public function setNavPageSize($pageLimit, $pageStart = false) {
        if ($pageStart) {
            $this->aNavParams = array('iNumPage' => $pageStart, "nPageSize" => $pageLimit, "bShowAll" => false);
        } else {
            $this->aNavParams = array("nPageSize" => $pageLimit, "bShowAll" => false);
        }
        return $this;
    }

    protected function prepareIds($ids) {
        if (is_array($ids)) {
            $ids = array_unique($ids);
            foreach ($ids as $key => $val) {
                $ids[$key] = intval($val);
            }
        } else {
            $ids = intval($ids);
        }
        return $ids;
    }

    public function getFilter() {
        return $this->aFilter;
    }

    public function getSelect() {
        return $this->aSelect;
    }

    public function getOrder() {
        return $this->aOrder;
    }
}