<?php

namespace Start\Iblock;

use Start\Image;

class ElementsDbResult
{

    /* @var \CIBlockResult */
    protected $dbResult = null;

    protected $bTextHtmlAuto = true;
    protected $bUseTilda = false;

    protected $previewSize = array('width' => 0, 'height' => 0, 'exact' => 0);
    protected $detailSize = array('width' => 0, 'height' => 0, 'exact' => 0);

    protected $fetchCallback = null;

    public function __construct(\CIBlockResult $dbResult) {
        $this->dbResult = $dbResult;
    }

    public function setDetailUrlTemplate($DetailUrl = "") {
        $this->dbResult->SetUrlTemplates($DetailUrl);
    }

    public function getCountItems() {
        return $this->dbResult->SelectedRowsCount();
    }

    public function getCountItemsOnPage() {
        return $this->dbResult->NavPageSize;
    }

    public function getNavString($template = '', $nPage = 10) {
        $this->dbResult->nPageWindow = $nPage;
        return $this->dbResult->GetPageNavStringEx($navComponentObject, "title", $template, false);
    }

    public function getNavCurrentPage() {
        return $this->dbResult->NavPageNomer;
    }

    public function getNavNum() {
        return $this->dbResult->NavNum;
    }

    public function getNavCountPages() {
        return $this->dbResult->NavPageCount;
    }

    protected function prepareFetch($aItem) {
        return $aItem;
    }

    public function fetch() {
        if ($aItem = $this->dbResult->GetNext($this->bTextHtmlAuto, $this->bUseTilda)) {

            if (!empty($aItem['PREVIEW_PICTURE'])) {
                $aItem['PREVIEW_PICTURE'] = Image::resizeImageById($aItem['PREVIEW_PICTURE'],
                    $this->previewSize['width'],
                    $this->previewSize['height'],
                    $this->previewSize['exact']
                );
            }

            if (!empty($aItem['DETAIL_PICTURE'])) {
                $aItem['DETAIL_PICTURE'] = Image::resizeImageById($aItem['DETAIL_PICTURE'],
                    $this->detailSize['width'],
                    $this->detailSize['height'],
                    $this->detailSize['exact']
                );
            }

            $aItem['SHOW_COUNTER'] = (!empty($aItem['SHOW_COUNTER'])) ? intval($aItem['SHOW_COUNTER']) : 0;

            $aItem = $this->prepareFetch($aItem);

            if ($this->fetchCallback) {
                $aItem = call_user_func($this->fetchCallback, $aItem);
            }

        }
        return $aItem;
    }

    public function setPreviewSize($width, $height, $exact = 0) {
        $this->previewSize = array('width' => intval($width), 'height' => intval($height), 'exact' => $exact);
        return $this;
    }

    public function setDetailSize($width, $height, $exact = 0) {
        $this->detailSize = array('width' => intval($width), 'height' => intval($height), 'exact' => $exact);
        return $this;
    }


    public function setFetchCallback($callback) {
        if (is_callable($callback)){
            $this->fetchCallback = $callback;
        }

        return $this;
    }

    public function setFetchParams($bTextHtmlAuto = true, $bUseTilda = false) {
        $this->bTextHtmlAuto = $bTextHtmlAuto;
        $this->bUseTilda = $bUseTilda;
        return $this;
    }

    public function fetchAll($indexKey = false) {
        $list = array();

        while ($aItem = $this->fetch()) {
            if ($indexKey) {
                $list[$aItem[$indexKey]] = $aItem;
            } else {
                $list[] = $aItem;
            }

        }

        return $list;
    }
}