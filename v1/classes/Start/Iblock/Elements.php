<?php

namespace Start\Iblock;

class Elements
{

    protected $dateMask = 'd.m.Y H:i:s';
    //protected $dateMask = 'Y-m-d H:i:s';

    protected $aOrder = array(
        'SORT' => 'ASC',
    );

    protected $aFilter = array(
        'CHECK_PERMISSIONS' => 'N'
    );

    protected $aGroupBy = false;

    protected $aSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'ACTIVE',
        'SHOW_COUNTER',
        'DATE_CREATE',
        'TIMESTAMP_X',
        'LANG_DIR',
        'IBLOCK_SECTION_ID',
        'DATE_ACTIVE_FROM',
        'DETAIL_PAGE_URL',
        'SECTION_PAGE_URL',
        'LIST_PAGE_URL',
    );

    protected $aNavParams = false;

    protected $dbResult = null;

    /* @return Elements */
    public static function create() {
        $class = get_called_class();
        return new $class;
    }

    /* @return \CDBResult */
    public function getDbResult() {
        return \CIBlockElement::GetList($this->aOrder, $this->aFilter, $this->aGroupBy, $this->aNavParams, $this->aSelect);
    }

    /* @return ElementsDbResult */
    public function execute() {
        if (empty($this->dbResult)) {
            $class = get_class($this) . 'DbResult';
            $class = class_exists($class, true) ? $class : 'Start\\Iblock\\ElementsDbResult';
            $iblockResult = $this->getDbResult();
            $this->dbResult = new $class($iblockResult);
        }
        return $this->dbResult;
    }

    public function executeCnt() {
        $this->aGroupBy = array();
        $cnt = $this->getDbResult();
        return $cnt;
    }

    public function getAdditionalCacheId() {
        $aNavParams = \CDBResult::GetNavParams($this->aNavParams);
        return serialize($this->aSelect) . '|' . serialize($this->aFilter) . '|' . serialize($this->aOrder) . '|' . serialize($aNavParams);
    }

    public function setIblockId($id) {
        $this->aFilter['IBLOCK_ID'] = intval($id);
        return $this;
    }

    public function mergeFilter($aFilter) {
        $this->aFilter = array_merge($this->aFilter, $aFilter);
        return $this;
    }

    public function mergeSelect($aSelect) {
        $this->aSelect = array_merge($this->aSelect, $aSelect);
        return $this;
    }

    public function unsetFilter($keys) {
        foreach ($keys as $val) {
            unset($this->aFilter[$val]);
        }
        return $this;
    }

    public function setOrder($aOrder) {
        $this->aOrder = $aOrder;
        return $this;
    }

    public function setIds($ids) {
        $this->aFilter['ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setCode($code) {
        $this->aFilter['=CODE'] = $code;
        return $this;
    }

    public function setExcludeIds($ids) {
        $this->aFilter['!ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setSectionId($ids) {
        $this->aFilter['SECTION_ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setSectionCode($code) {
        $this->aFilter['SECTION_CODE'] = $code;
        return $this;
    }

    public function setIncludeSubsections() {
        $this->aFilter['INCLUDE_SUBSECTIONS'] = 'Y';
        return $this;
    }

    public function setActive($active = 'Y') {
        $active = ($active && $active == 'N') ? 'N' : 'Y';
        $this->aFilter['ACTIVE'] = $active;
        return $this;
    }

    public function setElementCode($code) {
        return $this->setCode($code);
    }

    public function setCreatedUserId($id) {
        $this->aFilter['CREATED_USER_ID'] = $id;
        return $this;
    }

    public function setModifiedUserId($id) {
        $this->aFilter['MODIFIED_USER_ID'] = $id;
        return $this;
    }

    public function setNavTopCount($topLimit = false) {
        if ($topLimit) {
            $this->aNavParams = array('nTopCount' => $topLimit);
        } else {
            $this->aNavParams = false;
        }
        return $this;
    }

    public function setNavPageSize($pageLimit, $pageStart = false) {
        if ($pageStart) {
            $this->aNavParams = array('iNumPage' => $pageStart, "nPageSize" => $pageLimit, "bShowAll" => false);
        } else {
            $this->aNavParams = array("nPageSize" => $pageLimit, "bShowAll" => false);
        }
        return $this;
    }

    public function setNavElementId($id, $pageLimit = 1) {
        $this->aNavParams = array('nElementID' => $this->prepareIds($id), 'nPageSize' => $pageLimit);
        return $this;
    }

    public function addSelectVal($val) {
        if (!in_array($val, $this->aSelect)) {
            $this->aSelect[] = $val;
        }
    }

    public function removeSelectVal($val) {
        if (($key = array_search($val, $this->aSelect)) !== false) {
            unset($this->aSelect[$key]);
        }
    }

    protected function prepareIds($ids) {
        if (is_array($ids)) {
            $ids = array_unique($ids);
            foreach ($ids as $key => $val) {
                $ids[$key] = intval($val);
            }
        } else {
            $ids = intval($ids);
        }
        return $ids;
    }

    public function getFilter() {
        return $this->aFilter;
    }

    public function getSelect() {
        return $this->aSelect;
    }

    public function getNavParams() {
        return $this->aNavParams;
    }

    public function getOrder() {
        return $this->aOrder;
    }
}
