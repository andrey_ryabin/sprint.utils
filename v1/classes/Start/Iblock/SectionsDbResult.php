<?php

namespace Start\Iblock;

use Start\Image;

class SectionsDbResult
{

    /* @var \CIBlockResult */
    protected $dbResult = null;

    protected $bTextHtmlAuto = true;
    protected $bUseTilda = false;

    protected $pictureSize = array('width' => 0, 'height' => 0, 'exact' => 0);

    protected $fetchCallback = null;

    public function __construct(\CIBlockResult $dbResult) {
        $this->dbResult = $dbResult;
    }

    public function setPictureSize($width, $height, $exact = 0) {
        $this->pictureSize = array('width' => intval($width), 'height' => intval($height), 'exact' => $exact);
        return $this;
    }

    public function setDetailUrlTemplate($DetailUrl = "") {
        $this->dbResult->SetUrlTemplates("", $DetailUrl);
        return $this;
    }

    public function getCountItems() {
        return $this->dbResult->SelectedRowsCount();
    }

    public function fetch() {
        if ($aItem = $this->dbResult->GetNext($this->bTextHtmlAuto, $this->bUseTilda)) {

            $aItem['ELEMENT_CNT'] = !empty($aItem['ELEMENT_CNT']) ? intval($aItem['ELEMENT_CNT']) : 0;

            if (!empty($aItem['PICTURE'])) {
                $aItem['PICTURE'] = Image::resizeImageById($aItem['PICTURE'],
                    $this->pictureSize['width'],
                    $this->pictureSize['height'],
                    $this->pictureSize['exact']
                );
            }

            $aItem = $this->prepareFetch($aItem);

            if ($this->fetchCallback) {
                $aItem = call_user_func($this->fetchCallback, $aItem);
            }

        }
        return $aItem;
    }

    public function setFetchCallback($callback) {
        if (is_callable($callback)) {
            $this->fetchCallback = $callback;
        }

        return $this;
    }

    protected function prepareFetch($aItem) {
        return $aItem;
    }

    public function fetchAll($indexKey = false) {
        $list = array();

        while ($aItem = $this->fetch()) {
            if ($indexKey) {
                $list[$aItem[$indexKey]] = $aItem;
            } else {
                $list[] = $aItem;
            }

        }

        return $list;
    }
}
