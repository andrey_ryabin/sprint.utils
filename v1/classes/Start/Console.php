<?php

namespace Start;

class Console
{

    public function __construct() {
        //
    }

    public function executeFromArgs($args) {
        try {
            $this->executeFromArgsUnsafe($args);
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    protected function executeFromArgsUnsafe($args) {
        error_reporting(E_ERROR);

        if (empty($args)) {
            Throw new \Exception('args not found');
        }

        $first = array_shift($args);

        $command = array_shift($args);
        $tmp = explode(':', $command);
        if (count($tmp) < 3) {
            Throw new \Exception('format namespace:class:method arg1 arg2 .. argN');
        }

        list($ns, $task, $method) = $tmp;

        $method = 'execute' . $this->camelizeText($method);

        $ns = $this->camelizeText($ns);
        $task = $this->camelizeText($task);

        $class = $ns . '\\Tasks\\' . $task;

        if (!class_exists($class)) {
            Throw new \Exception('class ' . $class . ' not found');
        }

        if (!method_exists($class, $method)) {
            Throw new \Exception('method not found');
        }

        $obj = new $class;
        call_user_func_array(array($obj, $method), $args);
    }

    protected function camelizeText($str, $prefix = '') {
        $str = str_replace(array('_', '-', ' '), '*', $str);
        $str = explode('*', $str);

        $tmp = !empty($prefix) ? array($prefix) : array();
        foreach ($str as $val) {
            $tmp[] = ucfirst(strtolower($val));
        }

        return implode('', $tmp);
    }
}