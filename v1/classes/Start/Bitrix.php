<?php

namespace Start;

class Bitrix {

    /* @return \CMain */
    public static function getApplication(){
        return $GLOBALS['APPLICATION'];
    }

    /* @return \CUser */
    public static function getUser(){
        return $GLOBALS['USER'];
    }

    /* @return \CDatabase */
    public static function getDb(){
        return $GLOBALS['DB'];
    }
}