<?php

namespace Start\Rating;
use Start\Bitrix;

class UserRating {

    protected $category = 'default';

	public function addRate($userId, $elementId, $rate){
		$elementId = (int) $elementId;
		$userId = (int) $userId;

		$rate = (int) $rate;
		if ($rate < 1 || $rate > 5){
			$rate = 1;
		}

		if ($userId <= 0 || $elementId <= 0){
			return false;
		}

		if (!$this->addRateToDb($userId, $elementId, $rate)){
			return false;
		}

        return $this->getRate($elementId);
	}

    public function getRate($elementId){
		$result = $this->getRateFromDb($elementId);

        $result['rating'] = ($result['total_summ'] + 31.25) / ($result['total_cnt'] + 10);
        $result['rating'] = number_format($result['rating'], 4, '.', '');

        $result['category'] = $this->category;
        $result['element_id'] = $elementId;
        return $result;
    }

	public function getUserRates($userId, $elementIds){
		$elementIds = $this->prepareIds($elementIds);
        $userId = (int) $userId;

		if ($userId <= 0 || empty($elementIds)){
			return false;
		}

		$dbRes = Bitrix::getDb()->Query(startf('SELECT id, user_id, rate, element_id FROM start_user_rating
			WHERE user_id=%d AND element_id IN (%s) AND category="%s"',
            $userId, $this->getWhereIn($elementIds), $this->category
        ));

        $result = array();
        while ($item = $dbRes->Fetch()){
            $result[$item['element_id']] = $item['rate'];
        }

		return $result;
	}

	protected function getRateFromDb($elementId){
		$result = Bitrix::getDb()->Query(startf('SELECT SUM(rate) AS total_summ, COUNT(element_id) AS total_cnt
			FROM start_user_rating WHERE element_id="%d" AND rate > 0 AND category="%s"
			GROUP BY element_id', $elementId, $this->category
		))->Fetch();
		return $result;
	}

	protected function addRateToDb($userId, $elementId, $rate){
		$ok = Bitrix::getDb()->Query(startf('INSERT IGNORE INTO start_user_rating
			SET user_id="%d", element_id="%d", rate="%d", category="%s"',
			$userId, $elementId, $rate,$this->category
		))->AffectedRowsCount();
		return $ok;
	}

	protected function deleteRateToDb($userId, $elementId){
		$ok = Bitrix::getDb()->Query(startf('DELETE FROM start_user_rating
			WHERE user_id=%d AND element_id=%d AND category="%s"',
			$userId, $elementId, $this->category
		))->AffectedRowsCount();
		return $ok;
	}

	private function getWhereIn($ids){
		return implode(',', $ids);
	}

	private function prepareIds($ids){
		if (!is_array($ids)){
            $ids = array($ids);
		}

		$res = array();
		foreach ($ids as $val){
            $val = (int) $val;
            if ($val > 0){
                $res[] = $val;    
            }
		}

		$res = array_unique($res);
		return $res;
	}

	public function install(){
		Bitrix::getDb()->Query('CREATE TABLE IF NOT EXISTS start_user_rating (
				id int(10) unsigned NOT NULL AUTO_INCREMENT,
				user_id mediumint(8) unsigned NOT NULL,
				element_id mediumint(8) unsigned NOT NULL,
				rate tinyint(3) unsigned NOT NULL,
				category varchar(50) NOT NULL,
				PRIMARY KEY (id),
				UNIQUE KEY user_element_category (element_id,user_id,category)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;');
	}

	public function uninstall(){
		Bitrix::getDb()->Query('DROP TABLE IF EXISTS start_user_rating');
	}
}