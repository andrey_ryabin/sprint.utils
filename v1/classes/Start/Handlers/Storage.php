<?php

//AddEventHandler("iblock", "OnAfterIBlockUpdate", Array("Start\\Handlers\\Storage", "onAfterModifyIblock"));
//AddEventHandler("iblock", "OnAfterIBlockAdd", Array("Start\\Handlers\\Storage", "onAfterModifyIblock"));

namespace Start\Handlers;

class Storage {

	static public function onAfterModifyIblock(&$arFields){
		if (!empty($arFields['ID'])){
            \Start\Storage::getIblock()->clear();
		}
	}

}