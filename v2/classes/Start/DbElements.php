<?php

namespace Start;


abstract class DbElements
{

    protected $aOrder = array();
    protected $aFilter = array();
    protected $aSelect = array();
    protected $aNavParams = false;

    /** @var DbResult */
    private $dbresult = null;

    /** @return \CDBResult */
    abstract protected function createDbResult();

    public function fetchAll($callback = false, $indexKey = false) {
        return $this->getDbResult()->fetchAll($callback, $indexKey);
    }

    public function fetch($callback = false) {
        return $this->getDbResult()->fetch($callback);
    }

    /** @return DbResult */
    public function getDbResult() {
        if (!$this->dbresult) {
            $this->dbresult = $this->createDbResult();
        }
        return $this->dbresult;
    }

    public function mergeFilter($aFilter) {
        $this->aFilter = array_merge($this->aFilter, $aFilter);
        return $this;
    }

    public function mergeSelect($aSelect) {
        $this->aSelect = array_merge($this->aSelect, $aSelect);
        return $this;
    }

    public function setFilter($aFilter) {
        $this->aFilter = $aFilter;
        return $this;
    }

    public function setSelect($aSelect) {
        $this->aSelect = $aSelect;
        return $this;
    }

    public function setOrder($aOrder) {
        $this->aOrder = $aOrder;
        return $this;
    }

    public function setActive($active = 'Y') {
        $active = ($active && $active == 'N') ? 'N' : 'Y';
        $this->aFilter['ACTIVE'] = $active;
        return $this;
    }

    public function setNavTopCount($limit = false) {
        if ($limit) {
            $this->aNavParams = array('nTopCount' => $limit);
        } else {
            $this->aNavParams = false;
        }
        return $this;
    }

    public function setNavPageSize($limit, $offset = false) {
        if ($offset) {
            $this->aNavParams = array('iNumPage' => $offset, "nPageSize" => $limit, "bShowAll" => false);
        } else {
            $this->aNavParams = array("nPageSize" => $limit, "bShowAll" => false);
        }
        return $this;
    }

    public function setNavElementId($id, $limit = 1) {
        $this->aNavParams = array('nElementID' => $this->prepareIds($id), 'nPageSize' => $limit);
        return $this;
    }

    protected function prepareIds($ids) {
        if (is_array($ids)) {
            $ids = array_unique($ids);
            foreach ($ids as $key => $val) {
                $ids[$key] = intval($val);
            }
        } else {
            $ids = intval($ids);
        }
        return $ids;
    }
}