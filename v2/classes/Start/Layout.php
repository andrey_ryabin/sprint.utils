<?php

namespace Start;


class Layout
{

    public static function getPageLayout(){
        global $APPLICATION;
        $layoutFile = $_SERVER['DOCUMENT_ROOT'] . $APPLICATION->GetCurDir() . '.layout.php';

        $layout = array(
            'css' => '',
        );

        if (is_file($layoutFile)){
            $layoutExt = array();
            include $layoutFile;
            $layout = array_merge($layout, $layoutExt);
        }

        return $layout;
    }
}