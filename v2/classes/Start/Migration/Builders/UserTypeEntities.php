<?php

namespace Start\Migration\Builders;

use Start\Migration\Module;
use Start\Migration\AbstractBuilder;
use Start\Migration\HelperManager;
use Start\Migration\Exceptions\HelperException;

class UserTypeEntities extends AbstractBuilder
{

    public function initialize() {
        $this->setTitle('Экспорт пользовательских полей');
        $this->setDescription('');
        $this->setTemplateFile(__DIR__ . '/templates/UserTypeEntities.php');

        $this->setField('entity_id', array(
            'title' => '',
            'placeholder' => 'entity_id'
        ));
    }


    public function execute() {
        $helper = new HelperManager();

        $entityid = $this->getFieldValue('entity_id');
        $this->exitIfEmpty($entityid, 'entity_id empty');

        $entities = $helper->UserTypeEntity()->getUserTypeEntities($entityid);
        foreach ($entities as $index => $entity){
            unset($entity['ID']);
            $entities[$index] = $entity;
        }

        $this->setTemplateVar('entities', $entities);
    }
}