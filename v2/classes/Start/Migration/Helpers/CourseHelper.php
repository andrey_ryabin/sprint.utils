<?php

namespace Start\Migration\Helpers;

use Start\Migration\Helper;

class CourseHelper extends Helper
{

    public function __construct() {
        if (!\CModule::IncludeModule('learning')){
            $this->throwException(__METHOD__, 'learning module not installed');
        }
    }

    public function getCourse($code) {
        $item = \CCourse::GetList(array(), array(
            'CODE' => $code
        ), array())->Fetch();

        return ($item) ? $item : false;
    }

    public function getLesson($code) {
        $item = \CLearnLesson::GetList(array(), array(
            'CODE' => $code
        ))->Fetch();

        return ($item) ? $item : false;
    }

    public function getCourseId($code) {
        $item = $this->getCourse($code);
        return ($item) ? $item['LESSON_ID'] : 0;
    }

    public function addCourseIfNotExists($fields = array()) {
        $this->checkRequiredKeys(__METHOD__, $fields, array('CODE'));

        if ($item = $this->getCourse($fields['CODE'])) {
            return $item['ID'];
        }

        $course = new \CCourse;
        if ($id = $course->Add($fields)) {
            return $id;
        }

        $this->throwException(__METHOD__, 'error');
    }

    public function addLessionIfNotExists($courseId, $fields = array()) {
        $this->checkRequiredKeys(__METHOD__, $fields, array('CODE'));

        if ($item = $this->getLesson($fields['CODE'])) {
            return $item['ID'];
        }

        foreach ($fields as $k => $v) {
            if ($k == 'SORT') {
                $arProps[$k] = $v;
            } else {
                $arFields[$k] = $v;
            }
        }

        $props = array('SORT' => 500);
        if (isset($fields['SORT'])) {
            $props['SORT'] = $fields['SORT'];
            unset($fields['SORT']);
        }

        $lessonId = \CLearnLesson::Add(
            $fields,
            false,
            $courseId,
            $props
        );

        if ($lessonId) {
            return $lessonId;
        }

        $this->throwException(__METHOD__, 'error');
    }

}