<?php

namespace Start\Migration\Helpers;

use Start\Migration\Helper;
use Start\Migration\Helpers\UserTypeEntityHelper;

class UserTypeEntity2Helper extends UserTypeEntityHelper
{

    public function updateUserTypeEntityIfExists($entityId, $fieldName, $fields) {
        /* @global $APPLICATION \CMain */
        global $APPLICATION;

        $aItem = $this->getUserTypeEntity($entityId, $fieldName);
        if (!$aItem){
            return false;
        }

        $fields['FIELD_NAME'] = $fieldName;
        $fields['ENTITY_ID'] = $entityId;

        $entity = new \CUserTypeEntity;

        if ($entity->Update($aItem['ID'], $fields)) {
            return true;
        }

        if ($APPLICATION->GetException()) {
            $this->throwException(__METHOD__, $APPLICATION->GetException()->GetString());
        } else {
            $this->throwException(__METHOD__, 'UserType %s not updated', $fieldName);
        }
    }

}
