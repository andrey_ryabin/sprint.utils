<?php

namespace Start\Migration\Helpers;

use Start\Migration\Helper;

class UserHelper extends Helper
{


    public function authorize() {
        if (php_sapi_name() == 'cli') {
            $this->authorizeAdmin();
        }
    }

    protected function authorizeAdmin() {
        $group = \CGroup::GetList($by, $order, array(
            'ADMIN' => 'Y',
            'ACTIVE' => 'Y'
        ))->Fetch();

        if (empty($group)) {
            $this->throwException(__METHOD__, 'Admin group not found');
        }

        $by = 'id';
        $order = 'asc';

        $item = \CUser::GetList($by, $order, array(
            'GROUPS_ID' => array($group['ID']),
            'ACTIVE' => 'Y'
        ), array(
            'NAV_PARAMS' => array('nTopCount' => 1)
        ))->Fetch();


        if (empty($item)) {
            $this->throwException(__METHOD__, 'Admin user not found');
        }

        global $USER;

        if (!$USER->Authorize($item['ID'])) {
            $this->throwException(__METHOD__, 'Authorize error');
        }

    }


}