<?php

namespace Start;

/**

 *
 */
class IblockElements extends DbElements
{

    protected $aGroupBy = false;

    public function __construct($filter = array()) {
        $this->setOrder(array(
            'ID' => 'ASC',
        ));
        $this->mergeFilter(array(
            'CHECK_PERMISSIONS' => 'N'
        ));
        $this->mergeFilter($filter);
        $this->mergeSelect(array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'CODE',
            'ACTIVE',
            'SECTION_PAGE_URL',
            'IBLOCK_SECTION_ID',
            'SHOW_COUNTER',
            'DATE_CREATE',
            'TIMESTAMP_X',
            'LANG_DIR',
            'DATE_ACTIVE_FROM',
            'DETAIL_PAGE_URL',
            'LIST_PAGE_URL'
        ));
    }

    protected function createDbResult() {
        return new IblockElementsDbResult(
            \CIBlockElement::GetList(
                $this->aOrder,
                $this->aFilter,
                $this->aGroupBy,
                $this->aNavParams,
                $this->aSelect
            ));
    }

    public function setIblockId($id) {
        $this->aFilter['IBLOCK_ID'] = intval($id);
        return $this;
    }

    public function setSectionId($ids) {
        $this->aFilter['SECTION_ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setExcludeIds($ids) {
        $this->aFilter['!ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setIds($ids) {
        $this->aFilter['ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setCode($code) {
        $this->aFilter['=CODE'] = $code;
        return $this;
    }

    public function setSectionCode($code) {
        $this->aFilter['SECTION_CODE'] = $code;
        return $this;
    }

    public function setIncludeSubsections() {
        $this->aFilter['INCLUDE_SUBSECTIONS'] = 'Y';
        return $this;
    }

    public function setCreatedUserId($id) {
        $this->aFilter['CREATED_USER_ID'] = $id;
        return $this;
    }

    public function setModifiedUserId($id) {
        $this->aFilter['MODIFIED_USER_ID'] = $id;
        return $this;
    }

}