<?php

namespace Start;

class Component extends \CBitrixComponent
{

    /** @return \CMain */
    public function getApp() {
        return $GLOBALS['APPLICATION'];
    }

    public function resizeImage($image, $resizeParams = array()) {
        $resizeParams = array_merge(array(
            'width' => 0,
            'height' => 0,
            'exact' => 0,
            'init_sizes' => false,
            'filters' => false,
            'immediate' => false,
            'jpg_quality' => false,
        ), $resizeParams);

        if (is_numeric($image)) {
            $image = \CFile::GetFileArray($image);
        }

        if ($resizeParams['exact']) {
            $resizeType = BX_RESIZE_IMAGE_EXACT;
        } else {
            $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL;
        }

        if ($image && empty($image['SRC'])) {
            $image['SRC'] = \CFile::GetFileSRC($image);
        }

        if ($resizeParams['width'] > 0 && $resizeParams['height'] > 0) {

            $size = array(
                "width" => $resizeParams['width'],
                "height" => $resizeParams['height']
            );

            $resized = \CFile::ResizeImageGet(
                $image,
                $size,
                $resizeType,
                $resizeParams['init_sizes'],
                $resizeParams['filters'],
                $resizeParams['immediate'],
                $resizeParams['jpg_quality']
            );

            $image = array(
                "ID" => $image["ID"],
                "WIDTH" => $resized["width"],
                "HEIGHT" => $resized["height"],
                "SRC" => $this->urlencodePath($resized["src"]),
                "ORIGIN_SRC" => $this->urlencodePath($image['SRC']),
            );
        }
        return $image;
    }

    protected function urlencodePath($path) {
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        $len = count($parts);
        if ($len > 0) {
            $parts[$len - 1] = rawurlencode($parts[$len - 1]);
            $path = implode(DIRECTORY_SEPARATOR, $parts);
        }
        return $path;
    }

    public static function executeComponentAjaxByName($componentName, $ns = 'start') {
        $componentName = preg_replace('/[^a-z0-9\.\:]/', '', trim($componentName));

        $tmp = str_replace(array(':', '.'), '*', $componentName . '.component');
        $tmp = explode('*', $tmp);

        if (empty($tmp)) {
            return false;
        }

        if (count($tmp) <= 2) {
            return false;
        }

        if ($tmp[0] != $ns) {
            return false;
        }

        $classname = '';
        foreach ($tmp as $val) {
            $classname .= ucfirst($val);
        }

        self::includeComponentClass($componentName);
        if (!class_exists($classname)) {
            return false;
        }

        /** @var $obj \Start\Component */
        $obj = new $classname();
        $obj->initComponent($componentName);
        $obj->executeAjax();

    }


    public function executeAjax() {
        //
    }

    public function sendJson($data = array()) {
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        return $data;
    }

    protected function configureSefMode($arDefaultUrlTemplates404, $defaultPage = 'first') {
        $arVariables = array();
        $arUrlTemplates = \CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, array());
        $arVariableAliases = \CComponentEngine::makeComponentVariableAliases(array(), array());

        $engine = new \CComponentEngine($this);

        $componentPage = $engine->guessComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );

        $componentPage = ($componentPage) ? $componentPage : $defaultPage;
        \CComponentEngine::initComponentVariables($componentPage, array(), $arVariableAliases, $arVariables);

        $this->arResult = array(
            "SEF_FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases,
        );

        return $componentPage;
    }

}