<?php

namespace Start;


use Bitrix\Highloadblock as HL;


class Highloadblock
{

    /** @var  \Bitrix\Main\Entity\DataManager */
    private $dataclass = null;

    public function __construct($className) {
        $hlblock = HL\HighloadBlockTable::getList([
            'filter' => ['NAME' => $className],
        ])->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->dataclass = $entity->getDataClass();
    }

    protected function getDataClass() {
        return $this->dataclass;
    }


    public function deleteIfExists($filter = array()) {
        $dataclass = $this->getDataClass();
        $items = $this->findAll($filter);
        foreach ($items as $item) {
            $dataclass::delete($item['ID']);
        }
    }

    public function findAll($filter = array()) {
        $dataclass = $this->getDataClass();
        return $dataclass::getList([
            "select" => array('*'),
            "filter" => $filter
        ])->fetchAll();
    }

    public function findOnce($filter = array()) {
        $dataclass = $this->getDataClass();
        return $dataclass::getList([
            "select" => array('*'),
            "filter" => $filter
        ])->fetch();
    }


    public function addElement($data = array()) {
        $dataclass = $this->getDataClass();
        return $dataclass::add($data)->getId();
    }
}