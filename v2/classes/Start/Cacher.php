<?php

namespace Start;

class Cacher
{

    private $tags;

    private $cachePath;
    private $cacheId;

    private $lifetime;

    public function __construct($name, $params = array()) {
        $this->cachePath = "start/$name";
        $this->cacheId = serialize($params);
    }

    public static function clearByPath($name) {
        $cachePath = "start/$name";
        $phpCache = new \CPHPCache;
        $phpCache->CleanDir($cachePath);
    }

    public static function clearByTag($tag) {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag($tag);
    }

    public function setLifetime($lifetime) {
        $this->lifetime = $lifetime;
    }

    public function addTag($tag) {
        $this->tags[] = $tag;
        return $this;
    }

    public function store($callback, $lifetime = 3600) {
        $lifetime = ($this->lifetime) ? $this->lifetime : $lifetime;
        $phpCache = new \CPHPCache;
        $mValue = false;
        if ($phpCache->InitCache($lifetime, $this->cacheId, $this->cachePath)) {
            $mValue = $phpCache->GetVars();
        } elseif ($phpCache->StartDataCache($lifetime, $this->cacheId, $this->cachePath)) {
            $mValue = (is_callable($callback)) ? $callback() : false;
            $this->registerTags();
            $phpCache->EndDataCache($mValue);
        }
        return $mValue;
    }

    protected function registerTags() {
        if (!empty($this->tags)) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($this->cachePath);
            foreach ($this->tags as $tag) {
                $CACHE_MANAGER->RegisterTag($tag);
            }
            $CACHE_MANAGER->EndTagCache();
        }
    }

}
