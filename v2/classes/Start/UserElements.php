<?php

namespace Start;

/**
 * Class UserElements
 * @package Start
 *
 * @method getDbResult() UserElementsDbResult
 */
class UserElements extends DbElements
{

    public function __construct($filter = array()) {
        $this->mergeFilter($filter);
        $this->mergeSelect(array(
           'UF_*'
        ));
        $this->setOrder(array(
            'timestamp_x' => 'desc',
        ));
    }

    /* @return UserElementsDbResult */
    protected function createDbResult() {
        $by = current(array_keys($this->aOrder));
        $order = $this->aOrder[$by];

        $aParams = array();

        if (!empty($this->aSelect)) {

            $aFields = $aSelect = array();
            foreach ($this->aSelect as $val) {
                if (0 === strpos($val, 'UF')) {
                    $aSelect[] = $val;
                } else {
                    $aFields[] = $val;
                }
            }

            if (!empty($aSelect)) {
                $aParams['SELECT'] = $aSelect;
            }

            if (!empty($aFields)) {
                $aParams['FIELDS'] = $aFields;
            }
        }

        if (!empty($this->aNavParams)) {
            $aParams['NAV_PARAMS'] = $this->aNavParams;
        }


        return new UserElementsDbResult(
            \CUser::GetList(
                $by,
                $order,
                $this->aFilter,
                $aParams
            )
        );
    }


    public function setGroupIds($ids) {
        $this->aFilter['GROUPS_ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setIds($ids) {
        $this->aFilter['ID'] = $this->prepareUserIds($ids);
        return $this;
    }


    protected function prepareUserIds($ids) {
        if (is_array($ids)) {
            $ids = array_unique($ids);
            foreach ($ids as $key => $val) {
                $ids[$key] = intval($val);
            }

            $ids = implode('|', $ids);

        } else {
            $ids = intval($ids);
        }
        return $ids;
    }


}