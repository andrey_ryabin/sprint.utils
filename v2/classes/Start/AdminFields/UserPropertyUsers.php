<?php

//AddEventHandler('main', 'OnUserTypeBuildList');
namespace Start\AdminFields;

class UserPropertyUsers extends \CUserTypeInteger
{

    public function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "start_users",
            "CLASS_NAME" => "Start\\AdminFields\\UserPropertyUsers",
            "DESCRIPTION" => '[start] Привязка к пользователю',
            "BASE_TYPE" => "int",
        );
    }


    public function GetAdminListViewHTML($arUserField, $arHtmlControl) {
        return self::getUserTitle($arHtmlControl['VALUE']);
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl) {
        return startf('<input style="width: 100px;" type="text" name="%s" value="%s"/> %s',
            $arHtmlControl['NAME'],
            $arUserField['VALUE'],
            self::getUserTitle($arUserField['VALUE'])
        );
    }

    public static function getUserTitle($userid) {
        $userid = trim($userid);
        $userid = intval($userid);

        if ($userid > 0) {
            $item = \CUser::GetByID($userid)->Fetch();
            if ($item) {
                return $item['EMAIL'] . ' ' . $item['NAME'];
            }
        }

        return 'нет';

    }

}