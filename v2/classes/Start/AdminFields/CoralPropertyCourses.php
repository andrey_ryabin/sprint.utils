<?php

//AddEventHandler('main', 'OnUserTypeBuildList');
namespace Start\AdminFields;
use Start\Steps;


class CoralPropertyCourses extends \CUserTypeInteger
{

    public function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "start_courses_num",
            "CLASS_NAME" => "Start\\AdminFields\\CoralPropertyCourses",
            "DESCRIPTION" => '[coral] Привязка к курсу',
            "BASE_TYPE" => "int",
        );
    }


    public function GetAdminListViewHTML($arUserField, $arHtmlControl) {
        $elem1 = new Steps();
        $step = $elem1->getStepFromNavigation($arHtmlControl['VALUE']);

        return ($step) ? $step['name'] : 'нет';
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl) {
        $elem1 = new Steps();
        $courses = $elem1->getNavigation();

        ob_start();

        ?><select style="width: 150px;" name="<?=$arHtmlControl['NAME']?>">
        <option value="">нет</option>
        <?foreach ($courses as $course):?>
            <option <?if ($course['number'] == $arUserField['VALUE']):?>selected="selected"<?endif?> value="<?=$course['number']?>"><?=$course['name']?></option>
        <?endforeach?>
        </select><?
        $html = ob_get_clean();
        return $html;
    }

}