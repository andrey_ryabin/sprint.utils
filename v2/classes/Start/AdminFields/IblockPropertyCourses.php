<?php
//AddEventHandler('iblock', 'OnIBlockPropertyBuildList');

namespace Start\AdminFields;
use Start\LessonElements;


class IblockPropertyCourses
{
    
    private static $cacheCourses = array();
    
    public function GetUserTypeDescription() {

        return array(
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "start_courses",
            "DESCRIPTION" => '[start] Привязка к курсу (обучение)',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
        );
    }

    public function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){
        return $value['VALUE'];
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
        $courses = self::getLessonsSelect();
        ob_start();

        ?><select name="<?=$strHTMLControlName["VALUE"]?>">
        <option value="">нет</option>
        <?foreach ($courses as $course):?>
            <option <?if ($course['id'] == $value['VALUE']):?>selected="selected"<?endif?> value="<?=$course['id']?>"><?=$course['name']?></option>
        <?endforeach?>
        </select><?
        $html = ob_get_clean();
        return $html;
    }

    public function ConvertToDB($arProperty, $value) {
        return trim($value['VALUE']);
    }

    protected function getLessonsSelect(){
        if (!empty(self::$cacheCourses)){
            return self::$cacheCourses;
        }
        
        $elem1 = new LessonElements();

        $dbres1 = $elem1->setSelect(array('LESSON_ID', 'SORT', 'NAME'))
            ->setDepthLevel();

        self::$cacheCourses = array();
        while ($item1 = $dbres1->fetch()) {
            $step = array();
            $step['name'] = $item1['NAME'];
            $step['id'] = $item1['LESSON_ID'];

            self::$cacheCourses[] = $step;
        }

        return self::$cacheCourses;
    }

}