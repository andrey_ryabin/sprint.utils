<?php

//AddEventHandler('main', 'OnUserTypeBuildList');
namespace Start\AdminFields;

class CoralPropertyPractice extends \CUserTypeString
{

    public function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "start_practice",
            "CLASS_NAME" => "Start\\AdminFields\\CoralPropertyPractice",
            "DESCRIPTION" => '[coral] Практические действия',
            "BASE_TYPE" => "string",
        );
    }


    public function GetAdminListViewHTML($arUserField, $arHtmlControl) {
        return 'text';
    }

    public static function prepareValue($value){
        $value = trim($value);

        $value = htmlspecialchars_decode($value);
        $value = json_decode($value, true);

        $value = (json_last_error() == JSON_ERROR_NONE && is_array($value)) ? $value : array();
        return $value;
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl) {

        \CUtil::InitJSCore(Array("jquery"));

        ob_start();

        $value = self::prepareValue($arUserField['VALUE']);

        $jsonvalue = json_encode($value);

        $uniqid = 'start-practice-' . $arUserField['ID'];
        ?>
        <div class="<?= $uniqid ?>">
            <textarea class="sp-practice-res" name="<?= $arHtmlControl['NAME'] ?>"
                      style="display: none"><?= $jsonvalue ?></textarea>

            <table cellpadding="0" cellspacing="0" width="100%" class="internal">
                <tbody class="sp-practice-list">
                <tr class="heading">
                    <td align="center">Действия</td>
                    <td align="center" width="40">Баллы</td>
                </tr>
                </tbody>
            </table>

            <br/>
            <a href="#" class="sp-practice-add adm-btn">Добавить действие</a>

            <script type="text/template" class="sp-practice-item">
                <tr>
                    <td align="center" style="text-align:center;">
                        <textarea style="width: 98%">#TEXT#</textarea>
                    </td>
                    <td align="center" width="40" style="text-align:center;">
                        <input type="text" size="3" value="#SCORE#">
                    </td>
                </tr>
            </script>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                var $el = $('.<?=$uniqid?>');
                var jsonvalue = <?=$jsonvalue?>;
                var $form = $el.closest('form').first();
                var $plist = $el.find('.sp-practice-list');
                var $pres = $el.find('.sp-practice-res');
                var tpl = $el.find('.sp-practice-item').html();

                if (jsonvalue && jsonvalue.items) {
                    $.each(jsonvalue.items, function (index, item) {
                        $plist.append(
                            tpl.replace('#TEXT#', item.text)
                                .replace('#SCORE#', item.score)
                        );
                    });
                }

                $el.on('click', '.sp-practice-add', function (e) {
                    e.preventDefault();
                    $plist.append(
                        tpl.replace('#TEXT#', '').replace('#SCORE#', 10)
                    );
                });

                $form.on('submit', function (e) {
                    var postdata = {
                        items: []
                    };

                    $plist.find('tr').each(function () {
                        var text = $(this).find('textarea').val();
                        var score = $(this).find('input').val();

                        text = $.trim(text);
                        score = parseInt(score, 10);

                        if (text && score) {
                            postdata.items.push({
                                'text': text,
                                'score': score
                            });
                        }
                    });

                    $pres.val(
                        JSON.stringify(postdata)
                    );
                });
            });
        </script>
        <?
        $html = ob_get_clean();
        return $html;
    }

    public function GetDBColumnType($arUserField) {
        return "mediumtext";
    }

}