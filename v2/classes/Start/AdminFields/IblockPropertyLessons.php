<?php
//AddEventHandler('iblock', 'OnIBlockPropertyBuildList');

namespace Start\AdminFields;
use Start\LessonElements;


class IblockPropertyLessons
{
    
    private static $cacheLessons = array();
    
    public function GetUserTypeDescription() {

        return array(
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "start_lessons",
            "DESCRIPTION" => '[start] Привязка к теме курса (обучение)',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
        );
    }

    public function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){
        return $value['VALUE'];
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
        $courses = self::getLessonsSelect();
        ob_start();

        ?><select name="<?=$strHTMLControlName["VALUE"]?>">
        <option value="">нет</option>
        <?foreach ($courses as $course):?>
            <optgroup label="<?=$course['name']?>">
                <?foreach ($course['items'] as $item):?>
                    <option <?if ($item['id'] == $value['VALUE']):?>selected="selected"<?endif?> value="<?=$item['id']?>"><?=$item['name']?></option>
                <?endforeach;?>
            </optgroup>
        <?endforeach?>
        </select><?
        $html = ob_get_clean();
        return $html;
    }

    public function ConvertToDB($arProperty, $value) {
        return trim($value['VALUE']);
    }

    protected function getLessonsSelect(){
        if (!empty(self::$cacheLessons)){
            return self::$cacheLessons;
        }
        
        $elem1 = new LessonElements();

        $dbres1 = $elem1->setSelect(array('LESSON_ID', 'SORT', 'NAME'))
            ->setDepthLevel();

        self::$cacheLessons = array();
        while ($item1 = $dbres1->fetch()) {
            $step = array();
            $step['name'] = $item1['NAME'];
            $step['items'] = array();

            $elem1 = new LessonElements();
            $dbres2 = $elem1->setSelect(array('LESSON_ID', 'SORT', 'NAME'))
                ->setParentId($item1['LESSON_ID']);

            while ($item2 = $dbres2->fetch()) {
                $lesson = array();
                $lesson['id'] = $item2['LESSON_ID'];
                $lesson['name'] = $item1['NAME'] . '-' . $item2['NAME'];
                $step['items'][] = $lesson;
            }

            self::$cacheLessons[] = $step;
        }

        return self::$cacheLessons;
    }

}