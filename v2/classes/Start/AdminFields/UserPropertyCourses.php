<?php

//AddEventHandler('main', 'OnUserTypeBuildList');
namespace Start\AdminFields;
use Start\LessonElements;


class UserPropertyCourses extends \CUserTypeInteger
{

    public function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "start_courses",
            "CLASS_NAME" => "Start\\AdminFields\\UserPropertyCourses",
            "DESCRIPTION" => '[start] Привязка к курсу (обучение)',
            "BASE_TYPE" => "int",
        );
    }


    public function GetAdminListViewHTML($arUserField, $arHtmlControl) {
        return $arHtmlControl['VALUE'];
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl) {

        $courses = self::getLessonsSelect();
        ob_start();

        ?><select style="width: 150px;" name="<?=$arHtmlControl['NAME']?>">
        <option value="">нет</option>
        <?foreach ($courses as $course):?>
            <option <?if ($course['id'] == $arUserField['VALUE']):?>selected="selected"<?endif?> value="<?=$course['id']?>"><?=$course['name']?></option>
        <?endforeach?>
        </select><?
        $html = ob_get_clean();
        return $html;
    }

    private static $cacheCourses = array();

    protected function getLessonsSelect(){
        if (!empty(self::$cacheCourses)){
            return self::$cacheCourses;
        }

        $elem1 = new LessonElements();

        $dbres1 = $elem1->setSelect(array('LESSON_ID', 'SORT', 'NAME'))
            ->setDepthLevel();

        self::$cacheCourses = array();
        while ($item1 = $dbres1->fetch()) {
            $step = array();
            $step['name'] = $item1['NAME'];
            $step['id'] = $item1['LESSON_ID'];

            self::$cacheCourses[] = $step;
        }

        return self::$cacheCourses;
    }
}