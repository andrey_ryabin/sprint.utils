<?php
//AddEventHandler('iblock', 'OnIBlockPropertyBuildList');

namespace Start\AdminFields;

class IblockPropertyCheckbox
{
    public function GetUserTypeDescription() {

        return array(
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "start_checkbox",
            "DESCRIPTION" => '[start] Простое Да/нет',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
        );
    }

    public function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){
        return ($value['VALUE'] == '1') ? 'Да' : 'Нет';
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
        $yeschecked = $nochecked = '';
        if (intval($value['VALUE'] == '1')){
            $yeschecked = 'selected="selected"';
        } else {
            $nochecked = 'selected="selected"';
        }

        return startf('<select name="%s"><option %s value="1">да</option><option %s value="">нет</option></select>', $strHTMLControlName["VALUE"], $yeschecked, $nochecked);
    }

    public function ConvertToDB($arProperty, $value) {
        return ($value['VALUE'] == '1') ? '1' : '';
    }

}