<?php

namespace Start;

class Loader
{
    private $includePath;

    public function __construct($includePath = null) {
        $this->includePath = $includePath;
    }

    public function register() {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function unregister() {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    public function loadClass($className) {
        $fileName = '';
        if (false !== ($lastNsPos = strripos($className, '\\'))) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= $className . '.php';

        $fileName = $this->includePath . DIRECTORY_SEPARATOR . $fileName;
        if (is_readable($fileName)) {
            require $fileName;
        }


    }
}