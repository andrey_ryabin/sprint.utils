<?php

namespace Start;

class Tools
{

    private static $localCache = array();

    public static function getIblockId($code) {
        if (!isset(self::$localCache[$code])) {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $iblock = \CIBlock::GetList(array(
                'SORT' => 'ASC'
            ), array(
                'CHECK_PERMISSIONS' => 'N',
                '=CODE' => $code

            ))->Fetch();

            self::$localCache[$code] = ($iblock) ? $iblock['ID'] : 0;
        }
        return self::$localCache[$code];
    }

    public static function makeFile($fileId) {
        $fileId = intval($fileId);
        if ($fileId <= 0) {
            return false;
        }

        $file = \CFile::GetFileArray($fileId);
        if (!$file) {
            return false;
        }

        $ext = pathinfo($file['SRC'], PATHINFO_EXTENSION);
        if (in_array($ext, array('pdf', 'odt', 'odp', 'ods'))) {
            $file['VIEWERJS_SRC'] = '/ViewerJS/#' . $file['SRC'];
        }


        if (in_array($ext, array('pdf'))) {
            $file['MEDIAJS_SRC'] = $file['SRC'];
        }

//        $schema = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
//        $siteurl = $schema . '://' . $_SERVER['HTTP_HOST'];
//        if (in_array($ext, array('doc', 'docx', 'xls', 'xlsx'))) {
//            $file['VIEWERJS_SRC'] = 'https://docs.google.com/viewer?' . http_build_query(array(
//                    'url' => $siteurl . $file['SRC'],
//                    'embedded' => 'true',
//                ));
//        }

        return $file;
    }

    public static function makeFilesArray($fileIds = array()) {
        if (!is_array($fileIds)) {
            $fileIds = array($fileIds);
        }

        $files = array();
        foreach ($fileIds as $fileId) {
            if ($file = self::makeFile($fileId)) {
                $files[] = $file;
            }
        }

        return $files;
    }

    public static function makeVideosArray($videoLinks = array()) {
        if (!is_array($videoLinks)) {
            $videoLinks = array($videoLinks);
        }

        $videos = array();
        foreach ($videoLinks as $videoLink) {
            if (preg_match('%https?://vimeo.com/([0-9]+)%i', $videoLink, $match)) {
                $videos[] = array(
                    'code' => $match[1],
                    'type' => 'vimeo',
                );
            } elseif (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                $videoLink, $match)) {
                $videos[] = array(
                    'code' => $match[1],
                    'type' => 'youtube',
                );
            }
        }

        return $videos;
    }

}