<?php

namespace Start;


class DbResult
{

    /* @var \CDBResult */
    protected $dbResult = null;

    protected $bTextHtmlAuto = true;
    protected $bUseTilda = false;

    public function __construct(\CDBResult $dbResult) {
        $this->dbResult = $dbResult;
    }

    public function getSelectedRowsCount() {
        return $this->dbResult->SelectedRowsCount();
    }

    public function getNavPageSize() {
        return $this->dbResult->NavPageSize;
    }

    public function getNavPageString($template = '', $nPage = 10) {
        $this->dbResult->nPageWindow = $nPage;
        return $this->dbResult->GetPageNavStringEx(
            $navComponentObject,
            "title",
            $template,
            false
        );
    }

    public function getNavPageNum() {
        return $this->dbResult->NavPageNomer;
    }

    public function getNavPageCount() {
        return $this->dbResult->NavPageCount;
    }

    public function fetch($callback = false, $index = 0) {
        if ($item = $this->dbResult->GetNext($this->bTextHtmlAuto, $this->bUseTilda)) {
            if ($callback && is_callable($callback)) {
                $item = call_user_func_array($callback, array($item, $index));
            }
        }
        return $item;
    }

    public function fetchAll($callback = false, $indexKey = false) {
        $list = array();
        $index = 0;
        while ($item = $this->fetch($callback, $index)) {
            $index++;

            if ($indexKey) {
                $list[$item[$indexKey]] = $item;
            } else {
                $list[] = $item;
            }
        }

        return $list;
    }

    public function setFetchParams($bTextHtmlAuto = true, $bUseTilda = false) {
        $this->bTextHtmlAuto = $bTextHtmlAuto;
        $this->bUseTilda = $bUseTilda;
    }


}