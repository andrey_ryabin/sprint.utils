<?php

//AddEventHandler('main', 'OnBeforeEndBufferContent', array('\\Start\\AdminPanel', 'initializeAdminPanel'), 200);

namespace Start;

class AdminPanel
{

    protected static $buttonsToRemove = array();
    protected static $buttonsToAdd = array();
    
    public static function removeButtonsFileEdit(){
        self::removeButtons(array(
            'create',
            'create_section',
            'edit',
            'edit_section'
        ));
    }
    
    public static function getButtonsToRemove(){
        return self::$buttonsToRemove;
    }

    public static function getButtonsToAdd(){
        return self::$buttonsToAdd;
    }
    
    
    public static function removeButtons($codes) {
        self::$buttonsToRemove = array_merge(self::$buttonsToRemove, $codes);
    }

    public static function addButtonCreateNewElement($iblockId, $params = array()){
        $arButtons = \CIBlock::GetPanelButtons(
            $iblockId,
            0,
            0,
            array("SECTION_BUTTONS"=>false, "SESSID"=>false)
        );

        $id = 'start_pages_create_' . $iblockId;

        $default = array (
            'HREF' => $arButtons["edit"]["add_element"]["ACTION"],
            'TYPE' => 'BIG',
            'ID' => $id,
            'ICON' => 'bx-panel-create-page-icon',
            'ALT' => 'Создать страницу',
            'TEXT' => 'Создать#BR#страницу',
            'MAIN_SORT' => '100',
            'SORT' => 10,
            'RESORT_MENU' => true,
            'HK_ID' => 'top_panel_' . $id,
            'HINT' =>
                array (
                    'TITLE' => 'Создать страницу',
                    'TEXT' => 'Создать страницу',
                ),
        );

        $params = array_merge($default, $params);

        self::$buttonsToAdd[$id] = $params;
    }
    
    public static function addButtonEditElement($iblockId, $elementId, $params = array()){
        $arButtons = \CIBlock::GetPanelButtons(
            $iblockId,
            $elementId,
            0,
            array("SECTION_BUTTONS"=>false, "SESSID"=>false)
        );

        $id = 'start_pages_edit_' . $iblockId . '_' . $elementId;

        $default = array (
            'HREF' => $arButtons["edit"]["edit_element"]["ACTION"],
            'TYPE' => 'BIG',
            'ID' => $id,
            'ICON' => 'bx-panel-edit-page-icon',
            'ALT' => 'Редактировать страницу',
            'TEXT' => 'Редактировать#BR#страницу',
            'MAIN_SORT' => '200',
            'SORT' => 10,
            'RESORT_MENU' => true,
            'HK_ID' => 'top_panel_' . $id,
            'HINT' =>
                array (
                    'TITLE' => 'Редактировать страницу',
                    'TEXT' => 'Редактировать страницу',
                ),
        );

        $params = array_merge($default, $params);

        self::$buttonsToAdd[$id] = $params;
    }

    public static function initializeAdminPanel() {
        if (self::isEditMode()){

            $btnToRemove = self::getButtonsToRemove();
            $btnToAdd = self::getButtonsToAdd();

            global $USER, $APPLICATION, $adminPage;


            foreach ($btnToRemove as $code){
                unset($APPLICATION->arPanelButtons[$code]);
            }

            foreach ($btnToAdd as $code => $btn){
                $APPLICATION->arPanelButtons[$code] = $btn;
            }

        }
    }

    protected static function isEditMode() {
        global $USER;

        $isFrameAjax = \Bitrix\Main\Page\Frame::getUseHTMLCache() && \Bitrix\Main\Page\Frame::isAjaxRequest();
        if (isset($GLOBALS["USER"]) && is_object($USER) && $USER->IsAuthorized() && !isset($_REQUEST["bx_hit_hash"]) && !$isFrameAjax) {
            return true;
        }
        return false;
    }
}



