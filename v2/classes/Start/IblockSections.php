<?php

namespace Start;

/**
 * Class IblockElements
 * @package Start
 *
 * @method IblockSectionsDbResult getDbResult()
 *
 */
class IblockSections extends DbElements
{

    protected $bIncCnt = false;

    public function __construct($filter = array()) {
        $this->setOrder(array(
            'ID' => 'ASC'
        ));
        $this->mergeFilter(array(
            'CHECK_PERMISSIONS' => 'N'
        ));
        $this->mergeFilter($filter);
        $this->mergeSelect(array(
            'IBLOCK_ID',
            'ID',
            'NAME',
            'CODE',
            'ACTIVE',
            'SECTION_PAGE_URL',
            'PICTURE',
            'DEPTH_LEVEL',
            'IBLOCK_SECTION_ID'
        ));
    }

    protected function createDbResult() {
        return new IblockSectionsDbResult(
            \CIBlockSection::GetList(
                $this->aOrder,
                $this->aFilter,
                $this->bIncCnt,
                $this->aSelect,
                $this->aNavParams
            ));
    }

    public function setIblockId($id) {
        $this->aFilter['IBLOCK_ID'] = intval($id);
        return $this;
    }

    public function setSectionId($ids) {
        $this->aFilter['SECTION_ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setExcludeIds($ids) {
        $this->aFilter['!ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setIds($ids) {
        $this->aFilter['ID'] = $this->prepareIds($ids);
        return $this;
    }

    public function setCode($code) {
        $this->aFilter['=CODE'] = $code;
        return $this;
    }

    public function setOrderByTree() {
        $this->aOrder = array("left_margin" => "asc");
        return $this;
    }

    public function retrieveCountElements($activeOnly = 'Y') {
        $this->bIncCnt = true;
        $activeOnly = ($activeOnly && $activeOnly == 'Y') ? 'Y' : 'N';
        $this->mergeFilter(array('CNT_ACTIVE' => $activeOnly, 'ELEMENT_SUBSECTIONS' => 'Y'));
        return $this;
    }

    public function setDepthLevel($level = 1) {
        $level = (int)$level;
        $level = ($level >= 1) ? $level : 1;
        $this->aFilter['DEPTH_LEVEL'] = $level;
        return $this;
    }

}