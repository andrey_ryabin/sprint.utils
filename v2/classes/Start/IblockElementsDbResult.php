<?php

namespace Start;


class IblockElementsDbResult extends DbResult
{

    /* @var \CIBlockResult */
    protected $dbResult = null;

    public function __construct(\CIBlockResult $dbResult) {
        parent::__construct($dbResult);
    }

    public function setDetailUrlTemplate($DetailUrl = "") {
        $this->dbResult->SetUrlTemplates($DetailUrl);
    }

}