<?php

namespace Start;

class UserCurrent
{

    protected $user;
    protected $data = array();

    public function __construct(\CUser $user) {
        $this->user = $user;
        if ($user->GetID()) {
            $this->data = \CUser::GetByID(
                $user->GetID()
            )->Fetch();
        }
    }

    public function GetID() {
        return $this->user->GetID();
    }

    public function IsAuthorized() {
        return $this->user->IsAuthorized();
    }

    public function getFullName() {
        $name = array();
        if (!empty($this->data['NAME'])) {
            $name[] = $this->data['NAME'];
        }
        if (!empty($this->data['LAST_NAME'])) {
            $name[] = $this->data['LAST_NAME'];
        }

        if (!empty($this->data['SECOND_NAME'])) {
            $name[] = $this->data['SECOND_NAME'];
        }

        return implode(' ', $name);
    }

    public function getVal($name, $default = '') {
        return isset($this->data[$name]) ? $this->data[$name] : $default;
    }

    public function getValues($callback = false) {
        if ($callback && is_callable($callback)) {
            $data = call_user_func_array($callback, array($this->data));
        } else {
            $data = $this->data;;
        }
        return $data;
    }

}