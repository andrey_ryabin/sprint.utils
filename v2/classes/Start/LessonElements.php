<?php

namespace Start;


class LessonElements extends DbElements
{
    public function __construct() {
        $this->setOrder(array(
            'SORT' => 'ASC'
        ));

        $this->mergeFilter(array(
            'CHECK_PERMISSIONS' => 'N'
        ));

        $this->mergeSelect(array(
            'LESSON_ID',
            'SITE_ID',
            'WAS_CHAPTER_ID',
            'KEYWORDS',
            'CHILDS_CNT',
            'IS_CHILDS',
            'SORT',
            'TIMESTAMP_X',
            'DATE_CREATE',
            'CREATED_USER_NAME',
            'CREATED_BY',
            'ACTIVE',
            'NAME',
            'PREVIEW_PICTURE',
            'PREVIEW_TEXT',
            'PREVIEW_TEXT_TYPE',
            'DETAIL_TEXT',
            'DETAIL_PICTURE',
            'DETAIL_TEXT_TYPE',
            'LAUNCH',
            'CODE',
            'ACTIVE_FROM',
            'ACTIVE_TO',
            'RATING',
            'RATING_TYPE',
            'SCORM',
            'LINKED_LESSON_ID',
            'COURSE_ID',
            'COURSE_SORT',
            'UF_*',
        ));
    }

    public function setDepthLevel($level = 1) {
        $this->aFilter['>LINKED_LESSON_ID'] = 0;
        return $this;
    }

    public function setParentId($lessonId) {
        $this->aFilter['PARENT_ID'] = $lessonId;
        return $this;
    }

    protected function createDbResult() {
        if (isset($this->aFilter['PARENT_ID'])) {
            $lessonId = $this->aFilter['PARENT_ID'];
            unset($this->aFilter['PARENT_ID']);
            return new DbResult(
                \CLearnLesson::GetListOfImmediateChilds(
                    $lessonId,
                    $this->aOrder,
                    $this->aFilter,
                    $this->aSelect,
                    $this->aNavParams
                )
            );
        } else {
            return new DbResult(
                \CLearnLesson::GetList(
                    $this->aOrder,
                    $this->aFilter,
                    $this->aSelect,
                    $this->aNavParams
                )
            );
        }
    }


}